import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {HeaderMenu} from "./components/HeaderMenu";
import {BrowserRouter} from "react-router-dom";
import {Footer} from "./components/Footer/Footer";
import { ScrollButton } from './components/ScrollButton/ScrollButton';
import {isMobile} from 'react-device-detect'

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <BrowserRouter>
      <HeaderMenu isMobile={isMobile} />
      <App />
      <Footer isMobile={isMobile}/>
      {isMobile ? <></> : <ScrollButton />}
  </BrowserRouter>
);

