import {FC} from "react";
import { FAQGrid } from "../../components/FAQGrid";
import { InfoBadge } from "../../components/InfoBadge";
import {BaseLayout} from "../../components/layouts/BaseLayout";
import {MainBadge} from "../../components/MainBadge";
import { MoreBadge } from "../../components/MoreBadge";
import { Spacer } from "../../components/Spacer/Spacer";
import { SwipGrid } from "../../components/SwipGrid";
import { FAQ_INV, SUCCESS_STORY } from "../../data/constants";
import { PageProps } from "../../data/types";

export const InvestorsPage: FC<PageProps> = ({isMobile}) => {
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
            <MainBadge
                isMobile={isMobile}
                color='black100'
                textColor='white100'
                button="Pitch"
                withContact="Memo"                
                title='For Investors'
                text='Dear Investor, we are highly appreciated for your interest about Warp and want to share with you some information. To learn more about our product, our business model, our team and our roadmap and our finances please download pitch deck and investment memo created specially for you.'
            />
            <Spacer width={'100%'} height={isMobile ? 24 : 100} />
            <InfoBadge 
                title="You contact with us any time you like by " 
                isEmail
                isWu
                isTg
                color="black200"
                isMobile={isMobile}
            />
            <Spacer width={'100%'} height={isMobile ? 24 : 100} />
            <SwipGrid list={SUCCESS_STORY} isMobile={isMobile} />  
            <FAQGrid isMobile={isMobile} list={FAQ_INV}/>   
            <Spacer width={'100%'} height={isMobile ? 24 : 100} />
            <MoreBadge isMobile={isMobile}/>    
        </BaseLayout>
    )
}
