import {FC} from "react";
import {BaseLayout} from "../../components/layouts/BaseLayout";
import {FillBadge} from "../../components/FillBadge";
import {FAQGrid} from "../../components/FAQGrid";
import {MainBadge} from "../../components/MainBadge";
import { InfoGrid } from "../../components/InfoGrid";
import { FAQ, PARTNERS, PARTNERS_INFO, SUCCESS_STORY } from "../../data/constants";
import { Spacer } from "../../components/Spacer/Spacer";
import { MoreBadge } from "../../components/MoreBadge";
import { InfoBadge } from "../../components/InfoBadge";
import { BadgeGrid } from "../../components/BadgeGrid";
import { SwipGrid } from "../../components/SwipGrid";
import { PageProps } from "../../data/types";

export const PartnersPage: FC<PageProps> = ({isMobile}) => {
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
            <MainBadge
                isMobile={isMobile}
                color='black100'
                textColor="white100"
                button="Connect"
                title='For Partners and Suppliers'
                text='Warp is a best travel platform for all type of partners who needs of the seamless travel content at the best price and the efficiency way to earn together'
            />
            <Spacer width='100%' height={100}/>    
            <InfoBadge 
                isMobile={isMobile}
                title="Digital platform for all types of partners" 
                color="black100" />    
            <Spacer width='100%' height={20}/>                
            <BadgeGrid list={PARTNERS} isMobile={isMobile}/>
            <Spacer width='100%' height={36}/>
            <SwipGrid list={SUCCESS_STORY} isMobile={isMobile}/>     
            <Spacer width='100%' height={80}/>    
            <MainBadge
                isMobile={isMobile}
                color='black100'
                textColor="white100"
                title='Мain types of connections'
            />   
            <Spacer width='100%' height={36}/>  
            <InfoGrid list={PARTNERS_INFO} isMobile={isMobile}/>    
            <Spacer width='100%' height={42}/>  
            <FillBadge isMobile={isMobile}/>            
            <FAQGrid isMobile={isMobile} list={FAQ}/>
            <Spacer width='100%' height={42}/>  
            <MoreBadge isMobile={isMobile}/>
        </BaseLayout>
    )
}
