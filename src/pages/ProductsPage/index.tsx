import {FC, useState} from "react";
import {BaseLayout} from "../../components/layouts/BaseLayout";
import {PRODUCTS} from "../../data/constants";
import {FillBadge} from "../../components/FillBadge";
import {MainBadge} from "../../components/MainBadge";
import { BadgeGrid } from "../../components/BadgeGrid";
import { Products } from "../../components/Products";
import { Spacer } from "../../components/Spacer/Spacer";
import { useLocation } from "react-router-dom";
import { PageProps } from "../../data/types";
import {Text} from "../../components/Text/Text";
import { ChevronDown, ChevronUp } from "../../components/Icons";
import { theme } from "../../data/colors";

export const ProductsPage: FC<PageProps> = ({isMobile}) => {    
    const location = useLocation()
    const { type } = location.state as { type: number }
    const [bheight, setBheight] = useState('0px')
    const maxHeight = isMobile ? '1000px' : '400px'
    const handleHeight = () => {
        setBheight(bheight === '0px' ? maxHeight : '0px')
    }
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
            <MainBadge 
                isMobile={isMobile}
                color='black100' 
                textColor='white100'
                title={PRODUCTS[type].title}
                text={PRODUCTS[type].text} 
                button="Connect"
                withContact={PRODUCTS[type].title?.toLowerCase().includes('clipers') ? 'Demo' : undefined}
             />
            <Spacer width={'100%'} height={40} />            
            {PRODUCTS[type].title?.toLocaleLowerCase().includes('clipers')
                ? 
                <div style={{
                    display: 'flex',
                    width: '100%',
                    flexDirection: 'column',
                    marginTop: '10px',
                    justifyContent: 'center',
                    alignItems: 'center' 
                }}>
                    <Text size={isMobile ? 'title24' : 'title40'} colorTheme='white100' align='center' isBold>What else is important to <Text size={isMobile ? 'title24' : 'title40'} colorTheme='brand100' as='span' isBold>users</Text></Text>
                    <Spacer width={'100%'} height={8} />  
                    <div style={{
                        width: '70px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        border: `1px solid ${theme.colors.white300}`,
                        borderRadius: '12px'
                    }}
                    onClick={handleHeight}
                    >
                        {bheight === maxHeight ? <ChevronUp /> : <ChevronDown />}
                    </div>
                    <div style={{
                        height: bheight,
                        overflow: 'hidden',
                        transition: 'height 500ms ease',
                        width: '100%',
                        display: 'flex',
                        gap: '40px',
                        flexDirection: isMobile ? 'column' : 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: '10px'
                    }}>
                        <img alt="f1" src="/F_1.png" width={300} height={300}/>
                        <img alt="f1" src="/F_2.png" width={300} height={300}/>
                        <img alt="f1" src="/F_3.png" width={300} height={300} />
                    </div>
                </div> 
                :
                <BadgeGrid list={PRODUCTS[type].goods} isMobile={isMobile}/>
            }             
            <MainBadge
                isMobile={isMobile}
                color={isMobile ? 'black100' : 'black200'}
                textColor='white100'
                title='All Warp Products'
                text='Users & Partners benefit from their offers both in real and virtual worlds. They both get the same tools for planning travels and offering travel products using variety of Products built around Core platform benefiting from each other.'
            />    
            <Products list={PRODUCTS} isMobile={isMobile}/>               
            <Spacer width='100%' height={42}/>  
            <FillBadge isMobile={isMobile}/>
        </BaseLayout>
    )
}
