import styled from "@emotion/styled";

export const StyledWrapper = styled.div<{isMobile?:boolean}>`
    display: flex;
    width: ${({isMobile}) => isMobile ? `100%` : `81vw`};
    ${({isMobile}) => isMobile && `flex-direction: column;`}
`

export const StyledInnerWrapper = styled.div<{isMobile?:boolean}>`
    display: flex;
    flex-direction: column;
    width: ${({isMobile}) => isMobile ? `100%` : `50%`};
    justify-content: center;
`