import {ChangeEvent, FC, useEffect, useState} from "react";
import {BaseLayout} from "../../components/layouts/BaseLayout";
import { CONTACTS } from "../../data/constants";
import {Text} from "../../components/Text/Text";
import { MainBadge } from "../../components/MainBadge";
import { Spacer } from "../../components/Spacer/Spacer";
import { StyledInnerWrapper, StyledWrapper } from "./styled";
import { BaseField } from "../../components/fields/BaseField/BaseField";
import { BadgeGrid } from "../../components/BadgeGrid";
import { PageProps } from "../../data/types";
import { StyledConnectingButton } from "../MainPage/styled";
import { setCookie } from "../../data/cookie";
import { useLocation } from "react-router-dom";
import Alert from '@mui/material/Alert';
import { scrollToTop } from "../../utils/utils";


export const ContactPage: FC<PageProps> = ({isMobile}) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [description, setDescription] = useState('')
    const { pathname } = useLocation()
    const [modal, setModal] = useState(false)

    useEffect(() => {
        setCookie('app', 'cee71ed0-723a-11ed-a1eb-0242ac120002')
    }, [pathname])

    const saveData = async() => {
        try {
            // await axios.post("https://beta.clipers.keenetic.pro/api/warp", {
            //     name: name,
            //     email: email,
            //     description: description
            // },
            // {
            //     withCredentials: true,
            //     headers: {
            //         'Content-Type': 'application/json'
            //     }
            // })
            scrollToTop()
            setModal(true)
            const opt: RequestInit = {
                method: 'POST',
                headers: new Headers({'content-type': 'application/json'}),
                body: JSON.stringify({
                    name: name,
                    email: email,
                    description: description
                }),
                mode: 'no-cors'            
            }
            await fetch(`https://content.clipers.keenetic.pro/content/warp?name=${name}&email=${email}&description=${description}`, opt)
        } finally { 
            setTimeout(() => setModal(false), 700)       
        }
    }

    const handleClick = async () => {
        await saveData()
        setName('')
        setEmail('')
        setDescription('')
    }

    const handleChangeName = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value)        
    }

    const handleChangeEmail = (e: ChangeEvent<HTMLInputElement>) => {
        setEmail(e.target.value)        
    }

    const handleChangeDescription = (e: ChangeEvent<HTMLInputElement>) => {
        setDescription(e.target.value)        
    }

    const handleClearField = (type: 'name' | 'email' | 'description') => () => {
        if (type === 'name') setName('')
        else if (type === 'email') setEmail('')
        else setDescription('')
    }

    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
            <MainBadge
                isMobile={isMobile}
                color='black100'   
                textColor='white100'             
                title='Contact Warp'/>            
            <BadgeGrid list={CONTACTS} isMobile={isMobile}/>
            <Spacer width={'100%'} height={56}/>
            <StyledWrapper isMobile={isMobile}>    
                <StyledInnerWrapper isMobile={isMobile}>
                    <Text size='title64' colorTheme='white100' isBold align='left'>Feedback</Text>
                </StyledInnerWrapper>        
                <StyledInnerWrapper isMobile={isMobile}>
                    <Spacer width='100%' height={40} />
                    <Text size='text20' colorTheme='white100' align={isMobile ? 'center' : 'left'}>In case you have any questions or comments, we will be happy to answer and assist you. Please fill the form below and we will reply to you shortly.</Text>
                    <Spacer width='100%' height={24} />
                    <Text size='text20' colorTheme='white100' align='left'>Email</Text> 
                    <Spacer width='100%' height={12} />                   
                    <BaseField
                        onChange={handleChangeEmail}
                        value={email}
                        isMobile={isMobile}
                        name='email'
                        label='Enter your email'
                        onClearField={handleClearField('email')}
                    />
                    <Spacer width='100%' height={24} />
                    <Text size='text20' colorTheme='white100' align='left'>Name</Text>                    
                    <Spacer width='100%' height={12} />
                    <BaseField
                        onChange={handleChangeName}
                        value={name}
                        isMobile={isMobile}
                        name='email'
                        label='Enter your full name'
                        onClearField={handleClearField('name')}
                    />
                    <Spacer width='100%' height={24} />
                    <Text size='text20' colorTheme='white100' align='left'>Comments</Text>                    
                    <Spacer width='100%' height={12} />
                    <BaseField
                        onChange={handleChangeDescription}
                        value={description}
                        name='comments'
                        rows={3}
                        isMobile={isMobile}
                        label='Enter your comments'
                        onClearField={handleClearField('description')}
                        multiline
                    />
                    <Spacer width='100%' height={50} />
                    <StyledConnectingButton 
                        onClick={handleClick} 
                        isMobile={isMobile}>
                        <Text size={isMobile ? 'text16' : 'text20'} 
                              colorTheme='white100' 
                              align='center' 
                              isBold>
                            Send
                        </Text>
                    </StyledConnectingButton>
                </StyledInnerWrapper>
            </StyledWrapper>
            {modal ? 
            <Alert severity="success" color="info" style={{                
                width: '70%',
                position: 'absolute'         
            }}>
                application has been sent
            </Alert> : <></>
            }
        </BaseLayout>
    )
}
