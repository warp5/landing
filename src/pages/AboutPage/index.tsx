import {FC} from "react";
import { InfoGrid } from "../../components/InfoGrid";
import {BaseLayout} from "../../components/layouts/BaseLayout";
import {MainBadge} from "../../components/MainBadge";
import { Spacer } from "../../components/Spacer/Spacer";
import { ABOUT_INFO, TECHOLOGIES } from "../../data/constants";
import { StyledGrid, StyledLeftBlock, StyledRecordWrapper, StyledWrapper } from "./styled";
import {Text} from "../../components/Text/Text"
import { MoreBadge } from "../../components/MoreBadge";
import { PageProps } from "../../data/types";

export const AboutPage: FC<PageProps> = ({isMobile}) => {
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
            <MainBadge
                isMobile={isMobile}
                color='black100'
                textColor='white100'
                title='About Warp'
                text='Warp is a best travel platform for all type of partners who needs of the seamless travel content at the best price and the efficiency way to earn together.'
                button="Connect"
                />
            <Spacer width='100%' height={isMobile ? 60 : 120}/>
            <InfoGrid list={ABOUT_INFO} isMobile={isMobile}/>
            <Spacer width='100%' height={36}/>
            <MainBadge
                isMobile={isMobile}
                color='black100'    
                textColor='white100'            
                title='Proprietary Technology' />
            <StyledWrapper isMobile={isMobile}>
                {isMobile ? 
                <>
                    <StyledRecordWrapper isMobile>
                        <Text size='text16' colorTheme='black100' isBold align='center'>Very high loaded</Text>
                        <Spacer width='100%' height={8}/>
                        <Text size='text12' colorTheme='black100' isBold align='center'>{'> '} 20K requests</Text>
                        <Spacer width='100%' height={8}/>
                        <Text size='text12' colorTheme='black100' isBold align='center'>{'> '} 200 reservation per second</Text>
                    </StyledRecordWrapper>
                    {TECHOLOGIES.map(item => 
                        <StyledRecordWrapper key={item.title} isMobile>
                            <Text size='text16' colorTheme='black100' isBold align='center'>{item.title}</Text>
                            <Spacer width='100%' height={8}/>
                            {item.text ? <Text size='text12' colorTheme='black100' isBold align='center'>{item.text}</Text> : <></>}
                        </StyledRecordWrapper>
                    )}   
                </> : 
                <>
                    <StyledLeftBlock>                                        
                        <Text size='text20' colorTheme='black100' isBold align='left'>Very high loaded</Text>
                        <Text size='text16' colorTheme='black100' isBold align='left'>{'> '} 20K requests</Text>
                        <Text size='text16' colorTheme='black100' isBold align='left'>{'> '} 200 reservation per second</Text>
                    </StyledLeftBlock>
                    <StyledGrid>
                        {TECHOLOGIES.map(item => 
                            <StyledRecordWrapper key={item.title}>
                                <Text size='text20' colorTheme='black100' isBold align='left'>{item.title}</Text>
                                {item.text ? <Text size='text20' colorTheme='black100' isBold align='left'>{item.text}</Text> : <></>}
                            </StyledRecordWrapper>
                        )}
                    </StyledGrid>
                </>
                }                
            </StyledWrapper>                
            <Spacer width='100%' height={isMobile ? 50 : 150}/>
            <MainBadge
                isMobile={isMobile}
                color='black100'  
                textColor='white100'              
                title='Additional Information'
                text='Amet dapibus et. Vel leo, non dictum. In nunc velit urna non dui malesuada ornare ut. Ultricies. Mattis molestie orci, justo amet quis, sapien tempus consectetur habitasse habitasse faucibus. Nec vel in id dictum. Quis, pulvinar faucibus. Malesuada sodales eget non tempus nisi nec hac ut. Venenatis libero, dolor justo vestibulum et. Odio. Dolor sit dictum. Quis, hac habitasse nulla augue dictum sodales ultricies. Venenatis venenatis molestie libero, velit efficitur quam, dictumst'
            />
            <Spacer width='100%' height={60}/>
            <MoreBadge isMobile={isMobile}/>
        </BaseLayout>
    )
}
