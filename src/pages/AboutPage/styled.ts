import styled from "@emotion/styled"
import { theme } from "../../data/colors"
import { DefaultLogo } from "../../components/Icons"

export const StyledWrapper = styled.div<{isMobile?: boolean}>`
    display: flex;
    width: ${({isMobile}) => isMobile ? `100%` : `80vw`}; 
    flex-direction: ${({isMobile}) => isMobile ? `column` : `row`}; 
    gap: ${({isMobile}) => isMobile ? `12px` : `30px`};  
    ${({isMobile}) => !isMobile && `height: 650px;`}
     
`

export const StyledLeftBlock = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;    
    width: 20%;
    background: ${theme.colors.white300};
    border-radius: 50px;
    padding: 50px;
    height: 550px;
`

export const StyledGrid = styled.div`
  flex-direction: row;
  position: relative;
  place-items: left;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 30px;
  width: 80%;
  height: 550px;
`

export const StyledRecordWrapper = styled.div<{isMobile?: boolean}>`
    background: ${theme.colors.white300};
    border-radius: ${({isMobile}) => isMobile ? `12px` : `50px`};    
    padding: 50px;
    ${({isMobile}) => isMobile && `width: 100%;`}
`

export const StyledLogo = styled(DefaultLogo)`
    width: 300px;
    height: 220px;
`