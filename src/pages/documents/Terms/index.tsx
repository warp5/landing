import { FC } from "react";
import { BaseLayout } from "../../../components/layouts/BaseLayout";
import { PageProps } from "../../../data/types";
import {Text} from "../../../components/Text/Text";
import { Spacer } from "../../../components/Spacer/Spacer";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../../data/constants";

export const Terms: FC<PageProps> = ({isMobile}) => {
    const history = useNavigate()
    const goTo = (path: string) => () => {
        history(path)
    }
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
        <div>          
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            General provisions
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            Welcome to Warp (hereinafter referred to as "Service")!
            The service provided and controlled by Warp IT (hereinafter &ndash; &laquo;Owner&raquo;),
            accessible to the Internet user (hereinafter &ndash;
            &laquo;User&raquo;), includes a website for sharing
            videos, broadcasts, applications for mobile operating systems
            Android and iOS systems, software applications and other services, products
            and content (hereinafter referred to as "Services"). These Services
            provided for private, non-commercial use. Other
            the services offered by the Owner may be regulated by separate
            conditions.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            This User Agreement is legally
            binding agreement between the Owner and the User.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The use of the Service is carried out on the terms (hereinafter &ndash;
            &laquo; Conditions&raquo;) and in accordance with the rules set out in
            this User Agreement (hereinafter referred to as &mdash;
            &laquo;Agreement&raquo;, &laquo;User Agreement&raquo;).
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            Acceptance of the terms of the User Agreement
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            When using the Service or any other Services that are regulated by
            by these Terms and Conditions provided by the Owner, the User
            agrees to these Terms. By accessing the Services of the Service and
            any of its individual functions, or by going through the registration procedure,
            The User confirms that he concludes a legally binding
            agreement with the Owner, accepts this Agreement and undertakes
            to observe it. The User does not have the right to use the Service if
            disagreement with any of the provisions of this Agreement. Access
            The User's access to the Services and their use are also regulated by the Policy
            confidentiality and other rules of the Service, the terms of which
            presented directly in the Service.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            Changing the User Agreement
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The Agreement can be changed by the Owner without any special
            notifications. The new version of the Agreement comes into force from the moment of its
            placement on the Internet at the address specified in this paragraph,
            unless otherwise provided by the new version of the Agreement. Owner
            notifies the User of a change in the Agreement by
            sending him a corresponding message in the Service. Current
            the version of the Agreement is on the page at <span>address</span>{' '}
            <Text as='span' size={isMobile ? 'text16' : 'text20'} colorTheme='link' onClick={goTo(ROUTES.CONTACT)}>
              https://warp.travel/contact
            </Text>
            . If the User does not agree with the new version (updated
            version) The Agreement, then, for its part, the User must
            stop using the Services provided by the Service.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            User Registration. User Account
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            In order to use the Service, the User must pass
            the registration procedure, as a result of which the User will
            a unique personalized account has been created. When creating
            the User undertakes to provide accurate and
            up-to-date information. If the User provides an incorrect
            information or if the Owner has reason to believe that
            the information provided by the User is incomplete or unreliable,
            The owner has the right to unilaterally at his discretion
            block, suspend or delete a User account
            and deny the User further use of the Service, or its
            individual functions, including if the User does not comply with any
            from the provisions of this Agreement, or if the actions that
            carried out using a User account are considered
            By the Owner of illegal and violating the provisions of the current Agreement,
            or they may cause damage or worsen the quality of services or infringe,
            or violate the rights of third parties. User who has reached the age of 16
            years, has the right to independently register an account and
            use the Service within the legal capacity established by
            applicable law and limited by the current Agreement.
            Persons under the age of 18 may use the Services only with prior
            with the consent of your parent or legal guardian. You need to make sure,
            what the User's parent or legal guardian has read and discussed with
            By the User, these Terms and Conditions before the User starts
            use the Services of the Service. &nbsp;
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            When creating an account, the User must provide
            certain information, such as a mobile phone number
            User and password. The owner has the right to prohibit the use of logins
            with a certain set of characters, as well as set requirements for
            login and password (length, allowed characters, etc.). Authorization in
            The Service is carried out by the User by entering
            the mobile phone number and password specified during registration, or the code
            confirmation sent to the User by the Service in response to the input
            mobile phone numbers at the time of authorization.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            When creating an account, the User must provide
            certain information, such as a mobile phone number
            User and password. The owner has the right to prohibit the use of logins
            with a certain set of characters, as well as set requirements for
            login and password (length, allowed characters, etc.). Authorization in
            The Service is carried out by the User by entering
            the mobile phone number and password specified during registration, or the code
            confirmation sent to the User by the Service in response to the input
            mobile phone numbers at the time of authorization.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The Owner reserves the right at any time to require the
            User to confirm the data specified during account registration
            User records in the Service and containing information about the User,
            as well as other information related to the use of the Service.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The user is solely responsible for all performed
            actions in connection with the use of the Service under the account
            The User, including cases of voluntary transfer by the User
            means to access the User's account to third parties on
            any terms (including contracts or agreements).
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The user is solely responsible for security
            account data, as well as independently ensures their
            confidentiality.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            At the same time, all actions in connection with the use of the Service under the account
            the User's record is considered to be made by the User
            himself, except in cases when the User has notified the Owner about
            unauthorized access to the Service under a User account
            and/or about any violation (suspected violation) of confidentiality
            their means of access to the account (mobile phone numbers,
            login, password, etc.).
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            Access to the Services of the Service and the terms of their use
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            Access to the services of the Service and their use are regulated by this
            By agreement. The owner has the right to set restrictions on use
            Service for certain categories of Users depending on
            personalized conditions, such as geographical location
            The User, the language of use of the Service, the definition of the User to
            any of the internal categories of the Service. The user is hereby
            The Agreement gives the Owner consent to notify other Users
            The Service about the User's public actions, including the placement of
            of new publications, about the actions taken by him in relation to the content
            other Users, as well as about other activities performed by them in
            as part of the use of the Service.
          </Text>
          <Text size={isMobile ? 'text16' : 'text20'}>
            When using the Service, the User agrees that he does not have the right:
          </Text>
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              to access or use the Services of the Service, if not in
              is fully legal and capable to accept the present
              Agreement,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              generate unauthorized copies, modify, adapt,
              translate, decompile, disassemble, decode or
              otherwise extract the source code of the software
              Service, or create any derivative works based on
              The Services of the Service or any materials, including any files, tables or
              documentation (or part thereof), as well as attempt to restore the original
              code, algorithms, methods or techniques used in or
              derived from our services,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              to interfere or attempt to interfere with the operation of the Services of the Service, to violate
              operation of parts of the Service, including infrastructure and networks related to
              Service,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              use automated scripts to collect information or
              otherwise interact with the Services of the Service,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              impersonate another natural or legal person or
              a representative of an organization and/or community in the absence of
              appropriate powers, as well as apply any other forms and
              methods of illegal representation of other persons in the network, including
              by placing information in the Service contour by
              use of the Services of the Service,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              post materials, files containing or directly
              which are malicious programs (viruses, trojans, spam, worms,
              encoders, sniffers, spies and other categories of software
              software related to malware) or other malicious code
              for unauthorized access and
              unauthorized receipt of Service and User data
              Service,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              unauthorized collection and storage of any personal information
              third party, including addresses, phone numbers, addresses
              e-mail address, numbers and details of identification documents
              identity, details of bank payment instruments,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              distribute, license, transfer or sell, in full
              or in part, any of the Owner's Services or any derivatives thereof
              except in cases where the User has received such
              permission from the Owner, or when provided for
              the user agreement of any service Owner,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              to carry out placement:
            </Text>
          </ul>
          <ul style={{ listStyleType: 'disc', marginLeft: 43.4 }}>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              materials with pornographic images of minors and
              (or) ads about attracting minors as
              performers for participation in spectacular pornographic events
              character,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              materials and information constituting a state or other
              a secret specially protected by state law,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              materials containing public calls for implementation
              terrorist activities that publicly justify terrorism,
              other extremist materials, as well as materials promoting
              violence and brutality,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              materials containing obscene language,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              materials and information containing elements of violence and/or
              discrimination of a citizen or certain categories of citizens by
              based on gender, age, race or nationality,
              disability, language group, sexual orientation, attitudes to
              religion, profession, place of residence and work, as well as in connection with their
              political beliefs,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              information containing offers for retail sale by remote
              method of alcoholic beverages, and (or) alcohol-containing food
              products, and (or) ethyl alcohol, and (or) alcohol-containing
              non-food products, the retail sale of which is restricted or
              prohibited by legislation on state regulation
              production and turnover of ethyl alcohol, alcoholic and
              alcohol-containing products and restrictions on consumption (drinking)
              alcoholic beverages;
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              materials and information about ways to commit suicide, as well as
              calls to commit suicide,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              information that violates the requirements of the Federal Law of December 29
              2006 N 244-FZ "On state regulation of the activities of
              organization and conduct of gambling and on amendments to
              certain legislative acts of the Russian Federation" and the Federal
              the Law of November 11, 2003 No. 138-FZ "On Lotteries" on the prohibition of
              activities related to the organization and conduct of gambling and lotteries with
              using the Internet and other means of communication,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              information aimed at declination or other involvement
              minors in committing illegal actions,
              threatening their life and (or) health or for the life and
              (or) health of other persons,
            </Text>
            <Text size={isMobile ? 'text16' : 'text20'} as='li'>
              information about methods, methods of development, manufacture and
              the use of narcotic drugs, psychotropic substances and their
              precursors, new potentially dangerous psychoactive substances,
              places of their acquisition, methods and places of cultivation
              narcotic plants.
            </Text>
          </ul>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The owner reserves the right at any time and without prior notice
            notifying the User to perform a complete or partial deletion
            or disabling User access to Content at their discretion by
            for any reason or no reason.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            If the User suspects that any of the third parties
            an act of unauthorized use of the account was committed
            The User to access the Services of the Service, or confidentiality
            the account has been violated in any other way, it is necessary
            immediately independently carry out the end of the active session,
            go through the process of changing the account password and additionally notify
            The Owner of the fact of unauthorized access to the Service by
            the feedback form located at{' '}
            <Text as='span' size={isMobile ? 'text16' : 'text20'} colorTheme='link' onClick={goTo(ROUTES.CONTACT)}>
              https://warp.travel/contact
            </Text>
            .
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The owner is not responsible for any possible loss or damage
            data, as well as other consequences of any nature that may
            occur due to a violation by the User of the provisions of this Agreement,
            including points about unauthorized use of the account
            The user by third parties.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            During the User's use of the Service, the Owner aggregates
            information that is not personal data, because it does not allow
            directly or indirectly establish the identity of the User, but related to
            The user, his behavior patterns, tastes and preferences, etc.
            Such information is referred to as User Information and Owner
            collects, processes, stores and uses such
            information in accordance with the terms of the Privacy Policy,
            located at{' '}
            <Text as='span' size={isMobile ? 'text16' : 'text20'} colorTheme='link' onClick={goTo(ROUTES.POLICY)}>
              https://warp.travel/privacy-policy
            </Text>
            .
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The User has the right to terminate the use and carry out
            deleting your own account in the Service for any reason. In
            case of account deletion, reactivation of the account
            it may not be provided by the Owner and access to previously added
            User content may be restricted.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            Content
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            All proprietary information, objects that make up the Service,
            software, program code, databases, images,
            text, graphic images, illustrations, logos, patents,
            trademarks, service marks, copyright objects,
            photos, audio, video, music, appearance and all
            intellectual property rights associated with them posted on
            The Service (hereinafter referred to as the Content), are objects of exclusive rights
            The Owner and other relevant copyright holders. Using
            The content of the Service for any purposes not expressly permitted by this
            By agreement, it is strictly prohibited - such materials cannot be copied,
            reproduce, distribute, transmit, broadcast,
            demonstrate, sell, license or otherwise
            use for any purpose without the consent of the Owner.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The User, during the validity period of his account, has the right to
            to use the content of the Service exclusively within the framework of
            Service and only in ways directly implemented by
            standard functions of the Service. User's use of content
            The service is allowed provided that all of them are preserved unchanged
            copyright protection marks, related rights, trademarks, other
            notifications about authorship, saving the author's name (or pseudonym),
            the name of the copyright holder, the preservation of the corresponding object in
            unchanged. No elements of the Service, as well as any content,
            posted on the Service, cannot be used in any other way without
            prior permission of the Owner.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            Content provided to the User as part of the Services of the Service,
            provided for general information only. The content is not
            information that the User can definitely trust. Before
            than to take or refrain from any action, relying on
            solely on the content of the content obtained under our
            Services, the User should get professional advice
            specialist. The owner makes no representations or guarantees, explicit
            or implied that any content of the Service (including
            User Content) is accurate, complete, reliable or
            up-to-date. If the Services of the Service contain links to other sites and
            resources provided by third parties (including other
            by users of the Service), these links are provided exclusively for
            information. The owner does not control the content of the mentioned sites or
            other resources. These resource references cannot be interpreted
            as an approval of these sites or the information posted on them by
            The Owner, which the User can get through the transition
            according to them and subsequent studies. The User acknowledges and agrees,
            that the Owner is not obliged to check, control, view and
            edit any content of the Service (including User
            content) posted by the User or other users
            Service.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} colorTheme='black100'>
            User-Generated Content
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The services of the Service provide an opportunity for the User to create,
            upload, publish, transmit, receive and store or otherwise
            in this way, make available through our services, any text,
            photos, custom videos, sound recordings and music
            the works used in them, including videos that
            include locally saved audio recordings from personal music
            User libraries and other sounds (hereinafter referred to as "User
            content"). At the same time, the User retains all the existing ones
            he has exclusive rights to this content. However, the User
            grants the Owner a license to use this content.
            The scope of this license depends on what Services the User provides
            uses.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The User is solely responsible for the published and
            posted User content and for the consequences of conducting
            broadcasts, posting, uploading, publishing, transmitting or other
            ways to provide access to your own User
            content in the Service.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The owner has the right to delete, restrict access, move,
            edit or refuse to publish User Content,
            verified, containing or potentially containing at least
            one of the above violations of the current Agreement. Owner
            reserves the right to take appropriate measures to withdraw
            materials containing administrative and criminally punishable violations
            and penalties, including, but not limited to the list of violations by
            placement of materials specified earlier in the current Agreement.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            With respect to all User content resulting from
            intellectual activity and created by his creative,
            The User hereby grants the Owner
            , free of charge, on the terms of a simple (non-exclusive) license, the right to
            use by all means provided for in articles 1270, 1317 and
            1324 of the Civil Code of the Russian Federation, as well as by any other means that do not contradict the law,
            on the territory of all countries of the world during the entire validity period
            the exclusive right to such content, with the right to sublicense
            to third parties.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The Owner has the right to use the User's content without specifying the name
            (names) of the User, names (names) of the authors
            (copyright holders) of protected intellectual property results
            activities used as part of the User's content, including
            the names of the authors of the User's content (the right to anonymous use),
            as well as make changes, abbreviations and changes to the User's content.
            add-ons, provide User content when using it
            illustrations, preface, afterword, comments or any
            explanations. The owner has the right to carry out separate use
            sound and images recorded in the videos. The owner will not
            provide the User with a report on the use of content
            The user.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            Limitation of liability
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The Owner provides the Service to the User on an "as is" basis.
            The Owner does not guarantee the User the provision of the Service in
            uninterrupted and continuous operation. The owner does not guarantee
            To the User, the absence of errors in the provision of Services
            Service and is not responsible for any possible subsequent arising
            damage due to errors, but undertakes in case of detection
            take measures to eliminate them.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            The owner is not responsible for any losses, including
            any damage in the form of lost profits (incurred directly or indirectly),
            any loss of business reputation, any loss of data that occurred
            due to the User's use of the Services of the Service.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'text16' : 'text20'} as='h3' colorTheme='black100'>
            Other provisions
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            This Agreement is a legal agreement between
            By the User and the Owner and regulates the use by the User
            The services of the Service and completely replaces any previous agreements between
            By the User and the Owner in relation to the Services provided by the Service.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            This Agreement, its subject matter and content are governed by and
            interpreted in accordance with the legislation of the Russian Federation.
            Any disputes arising in connection with this Agreement relate to
            the exclusive jurisdiction of the courts of the Russian Federation and are resolved in
            in accordance with the procedure established by the current legislation of the Russian Federation
            Federation according to the norms of Russian law.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text16' : 'text20'}>
            If any of the provisions of this Agreement is recognized
            illegal, invalid or for any reason not having
            of legal force, then this provision will be limited or excluded from
            this Agreement to the minimum extent necessary and in no way
            will affect the validity and validity of all others
            provisions.
          </Text>
        </div>
        </BaseLayout>
    )
}