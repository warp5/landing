import { FC } from "react";
import { BaseLayout } from "../../../components/layouts/BaseLayout";
import { PageProps } from "../../../data/types";
import {Text} from "../../../components/Text/Text";
import { Spacer } from "../../../components/Spacer/Spacer";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../../data/constants";

export const Policy: FC<PageProps> = ({isMobile}) => {
    const history = useNavigate()
    const goTo = (path: string) => () => {
        history(path)
    }
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
        <div>          
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            General provisions
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text20'}>
            Welcome to Warp (hereinafter referred to as "Service")!
            The service provided and controlled by Warp IT (hereinafter &ndash; &laquo;Owner&raquo;),
            accessible to the Internet user (hereinafter &ndash;
            &laquo;User&raquo;), includes a website for sharing
            videos, broadcasts, applications for mobile operating systems
            Android and iOS systems, software applications and other services, products
            and content (hereinafter referred to as "Services"). These Services
            provided for private, non-commercial use. Other
            the services offered by the Owner may be regulated by separate
            conditions.
          </Text>
          <Text size={isMobile ? 'text12' : 'text16'}>
            This Privacy Policy (hereinafter referred to as the "Policy")
            applies to all information, including personal data in
            understanding of the applicable legislation (hereinafter referred to as "Personal data
            data&raquo;), which the Owner can receive from the User in
            during the User's use of any websites, programs, products
            and/or Services of the Service, as well as during the execution by the Owner of any
            agreements and contracts concluded with the User in connection with
            using the Services of the Service.
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            Who collects and processes User information
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            To enable the User to use the Services
            The Service collects and uses the User's personal data
            Limited Liability Company "Free Digital
            Solutions&raquo;, a legal entity established by law
            Of the Russian Federation and registered at the address: 125493, city
            Moscow, the wind.Golovinsky municipal district, Smolnaya str., 2,
            room. 5H/2, office 6 (LLC &laquo;SR&raquo;).
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            What information about the User is collected by the Owner
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The Owner collects and processes the information that the User
            provides in the process of using the Services of the Service. Collected
            The information may vary depending on whether
            the User uses a Service account to access the Services of the Service or
            not. Part of the Services of the Service requires the User to create an account,
            therefore, the Owner needs to receive from the User certain
            important information (registration data) such as date of birth, name
            user, password, email and phone number.
          </Text>
          <Spacer width='100%' height={12} />
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              information provided by the User during registration, such as
              alias, profile images, gender, hometown, information
              &laquo;about yourself&raquo;, information about social networks, education and
              career, etc.;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              user contacts of the address book (if allowed
              access by the User);
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              Information about network activity, such as browsing history,
              search history, videos or pages that the User visited,
              date and time of visits, accounts of other Users
              that the User has subscribed to, and information related to
              interaction with other Users within the Service;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              user-generated content such as comments, texts, messages,
              photos, images, videos, sounds, code or other data and
              materials uploaded by the User,
              distribution or broadcast when using the Services of the Service;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              technical data such as IP address, HTTP request headers,
              cookies, browser ID data, information about
              hardware and software ( such as the device model,
              operating system version, device memory capacity, advertising
              identifiers, unique identifiers of the application installed
              applications, unique device identifiers, data about
              device usage, browser type, installed keyboards,
              language, battery level and time zone), mobile network data
              and wi-fi, geolocation information;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              information provided to Users from other social networks
              Facebook Instagram, Google), when you create
              Warp account by connecting via another social network,
              or if you link a Warp account to a third-party
              a social network.
            </Text>
          </ul>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The owner accesses, stores, transmits and uses the information
            The user to create and distribute generalized data,
            which do not identify the User in any way. Generalized data can
            may be obtained from the User's personal information, but they are not considered
            personal information, since this data is neither directly nor indirectly
            reveal the identity of the User. For example, the Owner can collect
            put together the User's data on the use of the Services of the Service in order to
            calculate the percentage of users who use a certain
            the Service Services function, for creating statistics about Service users
            or to count ad impressions or ad clicks.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The Owner does not collect certain categories of personal information about
            the User (including information about race or ethnicity,
            religious or philosophical beliefs, political views,
            trade union membership, health information, as well as genetic and
            biometric data).
          </Text>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            What is the legal basis and purpose of Personal Data processing
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The Owner does not have the right to process the User's Personal Data without
            sufficient legal grounds for that. The owner processes
            Personal data of the User only if:
          </Text>
          <Spacer width='100%' height={12} />
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              processing is necessary to fulfill contracts between the Owner and
              By the User, such as the User agreement (
              <Text as='span' size={isMobile ? 'text12' : 'text16'} colorTheme='link' onClick={goTo(ROUTES.TERMS)}>
                https://warp.travel/user-agreement
              </Text>
              );
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              the requirement of the applicable legislation is implemented. For
              jurisdictions where consent is recognized as a separate legal basis,
              the beginning of the User's use of the Owner's Service expresses
              User's consent to such processing;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              access to the User account is being granted,
              if the User is registered in the relevant third-party
              services;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              communication is carried out with the User to send notifications,
              requests and information related to the operation of the Services of the Service,
              implementation of agreements with the User and processing of requests and requests,
              received from the User;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              the personalization of advertising and offers is carried out taking into account
              User preferences and other Personal Data of the User,
              provided to the Owner;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              improving the convenience of using the Services of the Service and providing
              more personalized offers of the content provided;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              creation of new Service Services;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              collection, processing and presentation of statistical and big data;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              identification of security threats to the Service, users, Company
              and/or third parties, including verification of the User's trustworthiness
              when concluding contracts using the Services of the Service.
            </Text>
          </ul>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            Where and for how long does the Owner store Personal Data about the User
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The Owner stores the User's Personal Data on
            servers located in the Russian Federation. The owner stores
            User's personal data as long as it is necessary for
            providing Services of the Service so that the Owner can
            to fulfill contractual obligations or for other legitimate purposes,
            such as compliance with the legal obligations of the Owner, permission
            disputes, and enforcement of agreements. If
            The User has expressed a desire to delete his
            own Personal Data previously posted by him from the Owner's databases, he can
            delete the necessary data yourself via the interface,
            the Service provided by the Services. The owner cannot guarantee,
            that the deletion of the User's Personal Data stores will be carried out within
            a strictly set time frame.
            There may be legitimate reasons for storing some User information. The owner also
            may suspend the removal procedure in case of obtaining a legitimate
            a court order demanding the preservation of the content or
            the receipt of reports of misuse or other
            violations of the User Agreement or Policy
            privacy, as well as if the User's account or content
            specified by other Users or algorithms of the Service as
            inappropriate or violating the terms of Service.
          </Text>

          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            How the Owner uses Personal Data about the User
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            Based on the User's personal data, the Owner performs:
          </Text>
          <Spacer width='100%' height={12} />
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              providing the User with personalized content,
              recommendation of new friends, possible acquaintances;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              improving the provision of current Service Services to the User and
              development of new;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              notification of the User about events and changes in the provision
              Services of the Service;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              detection of malicious, illegal and fraudulent activities,
              contrary to the rules of the User Agreement, in order to
              exceptions;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              ensuring user security, including verification
              User content, messages and related metadata
              for violation of the rules of the User Agreement and other
              unacceptable content that may be posted on the Service and
              provided through its Services;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              analysis and monitoring of trends and specifics of the use of Services
              Service;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              provision of Location-based Service Services
              The User (if the User provided earlier
              permission).
            </Text>
          </ul>
          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            Who is granted access to Personal Data about the User
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The Owner may transfer the User's Personal Data to its
            internal employees. The Owner can also transfer Personal
            User data to its affiliates, including others
            companies from the group of persons to which the Company belongs (hereinafter
            referred to as the "Group of Companies"). Using The Services Of The Service, The User
            agrees to such a transfer. The User's personal data may
            be transferred to persons belonging to the Company's Group and located with
            By a User in one country, to provide support to the User when
            using the Services of the Service. The Owner can transfer Personal
            User data to third parties outside the Company's Group.
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            Third parties in this context may include:
          </Text>
          <Spacer width='100%' height={12} />
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              partners, such as owners of websites and applications, advertising networks and
              other partners providing services related to
              placement and display of advertising on websites, in programs,
              products or services owned or
              controlled by such partners;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              advertisers or other Partners who display to the User
              advertising through the provision of Services of the Service, as well as such
              Partners as information service providers or consultants;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              persons providing information to identify security threats
              for the Service, users, the Company and/or third parties, including
              when verifying the User's trustworthiness when concluding contracts with
              using the Services of the Service;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              persons participating in the organization of the User's payment acceptance and
              conducting payment transactions using the Services of the Service
              (international payment systems, payment instrument providers,
              banks and other financial organizations, etc.).
            </Text>
          </ul>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            The Owner can also transfer the User's Personal Data
            to third parties outside the Company's Group:
          </Text>
          <Spacer width='100%' height={12} />
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              to third parties in respect of whom the assignment of rights or
              obligations has been made, or novation under the relevant agreement;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              to any national and/or international regulatory body,
              law enforcement agencies, central or local executive
              to the authorities, other official or state bodies or
              courts in respect of which the Company is obliged to provide information
              in accordance with the applicable legislation on the relevant
              request;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              to third parties, if the User has agreed to
              transfer of their Personal Data, or if the transfer of Personal
              the data is required to provide the User with the appropriate
              Services of the Service or the fulfillment of a certain agreement or contract,
              concluded with the User;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              to any third party in order to ensure the legal protection of the Company
              or third parties in case of violation by the User of the User
              agreement, this Policy, or in a situation where there is
              the threat of such a violation.
            </Text>
          </ul>

          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            Notification of Policy Changes
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            Periodically, the Owner can make changes to any
            the terms of this Policy. The owner has the right to make changes to
            at its sole discretion, including when appropriate changes are made
            related to changes in applicable legislation, as well as when
            the corresponding changes are related to changes in the operation of Services
            Service. The Owner can inform the User about
            making changes to the Privacy Policy by changing the date
            at the beginning of the corresponding text presented on the website and in
            the mobile offer. The owner can additionally notify
            The user about updates, for example, on the pages of a website or in
            in the application by sending notifications.
          </Text>

          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            User rights and capabilities
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>The User has the right to:</Text>
          <Spacer width='100%' height={12} />
          <ul style={{ listStyleType: 'square', marginLeft: '28.25px' }}>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              getting access to most of your profile information and
              editing it;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              deleting a User's uploaded Custom
              content;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              changing privacy and visibility settings, such as
              allowed and prohibited lists of video viewers
              users commenting on videos, sending private messages;
            </Text>
            <Text size={isMobile ? 'text12' : 'text16'} as='li'>
              revocation of permissions. In most cases, if the User
              agrees to the collection of information by the Owner,
              the User can subsequently withdraw his consent by changing
              settings of your application or device, if it provides
              such an opportunity. In this case, the User may lose access
              to some Services of the Service.
            </Text>
          </ul>

          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            Appeals
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            Requests concerning the processing of the User's Personal Data should be
            send to{' '}
            <Text as='span' size={isMobile ? 'text12' : 'text16'} colorTheme='link' onClick={goTo(ROUTES.CONTACT)}>
              https://warp.travel/contact
            </Text>
            .
          </Text>

          <Spacer width='100%' height={24} />
          <Text size={isMobile ? 'title32' : 'title40'} as='h1' colorTheme='white100'>
            Contact information
          </Text>
          <Spacer width='100%' height={12} />
          <Text size={isMobile ? 'text12' : 'text16'}>
            Questions, comments and suggestions regarding this Policy should be
            send to{' '}
            <Text as='span' size={isMobile ? 'text12' : 'text16'} colorTheme='link' onClick={goTo(ROUTES.CONTACT)}>
              https://warp.travel/contact
            </Text>{' '}
            or by using the appropriate feedback form in the mobile
            the application.
          </Text>
        </div>
        </BaseLayout>
    )
}