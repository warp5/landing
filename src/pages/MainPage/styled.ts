
import styled from "@emotion/styled"
import {theme} from "../../data/colors"
import { ChevronRight } from "../../components/Icons"
import {Text} from "../../components/Text/Text";

export const StyledInfoWrapper = styled.div<{bc?: keyof typeof theme['colors'], short?: boolean}>`
    background: ${({ bc }) => bc ? theme.colors[bc] : theme.colors.black100};
    border-radius: ${({short}) => short ? `50px` : `0`} ;
    width: ${({short, bc}) => short ? `70%` : (bc !== 'black100' ? `110%` : `100%`)};
    display: flex;
    gap: 20px;
    margin-top: 15px;
    ${({short}) => !short ? `justify-content: center; align-items: center;` : ``}
`

export const StyledInfoInnerWrapper = styled.div<{isStart?: boolean, isMobile?: boolean, isTg?: boolean}>`
    display: flex;
    flex-direction: column;   
    padding: ${({isMobile, isTg}) => isMobile ? (isTg ? `15px` : `0px`) : `30px 80px 0px 50px`};
    width: ${({isMobile}) => isMobile ? `100%` : `50%`};
    ${({isStart}) => !isStart ? `justify-content: center; align-items: center;` : `margin-bottom: 50px;`}    
`

export const StyledImageWrapper = styled.div`
    display: flex;
    flex-direction: column;   
    padding: 30px 80px 50px;
    width: 40%;
`

export const StyledConnectingButton = styled.div<{isMobile?: boolean}>`
    display: flex;
    justify-content: center;
    background: ${theme.colors.primary100};
    border-radius: ${({isMobile}) => isMobile ? `10px` : `16px`};   
    width: ${({isMobile}) => isMobile ? `100%` : `195px`};
    height: ${({isMobile}) => isMobile ? `44px` : `56px`};
    align-items: center; 
    justify-content: center;
`

export const StyledFillButton = styled.div<{isMobile?: boolean}>`
    display: flex;
    justify-content: center;
    border-radius: ${({isMobile}) => isMobile ? `10px` : `16px`};   
    width: ${({isMobile}) => isMobile ? `100%` : `195px`};
    height: ${({isMobile}) => isMobile ? `44px` : `56px`};
    align-items: center; 
    justify-content: center;
    border: 2px solid ${theme.colors.black500}    
`

export const StyledImage = styled.img`
    padding: 15px 24px 15px 32px;
    object-fit: cover;
`

export const StyledChevronRight = styled(ChevronRight)`

`

export const StyledText = styled(Text)`
  opacity: 0.64;
`

export const StyledClipersWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;    
    align-items: center;    
    justify-content: center;
    margin-top: 40px;
`

export const StyledClipersInnerWrapper = styled.div<{isMobile?: boolean}>`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;  
    width: 100%; 
    gap: ${({isMobile}) => isMobile ? '20' : '50'}px;   
`