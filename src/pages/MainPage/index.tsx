import {FC} from "react";
import {BaseLayout} from "../../components/layouts/BaseLayout";
import {FillBadge} from "../../components/FillBadge";
import {MainBadge} from "../../components/MainBadge";
import { Spacer } from "../../components/Spacer/Spacer";
import { INFO, LABELS, PRODUCTS } from "../../data/constants";
import { InfoGrid } from "../../components/InfoGrid";
import { Products } from "../../components/Products";
import { PageProps } from "../../data/types";

export const MainPage: FC<PageProps> = ({isMobile}) => {    
    return (
        <BaseLayout justifyContent='flex-start' backgroundColor='black100' isMobile={isMobile}>
            {isMobile ? 
                <>
                    <MainBadge
                        isMobile
                        color='black100'
                        title='Smart travel eco-system'
                        textColor='white100'
                        withContact='Contact Warp'
                        text='Warp is a unique travel eco-system where USERS can plan and manage all travel needs in one place in a truly new digital way and PARTNERS can seamlessly deliver their travel content to millions of customers at the best price in all possible variations, creating new “phygital” environment in travel & lifestyle industries'
                        button="Connect"
                        main
                    />
                    <Spacer width={'100%'} height={64} /> 
                    <MainBadge
                        color='black200'
                        isMobile
                        textColor='white100'
                        title='Warp Products'
                        text='Users & Partners benefit from their offers both in real and virtual worlds. They both get the same tools for planning travels and offering travel products using variety of Products built around Core platform benefiting from each other.'
                    />      
                    <Products list={PRODUCTS} isMobile/>    
                    <Spacer width={'100%'} height={64} /> 
                    <MainBadge
                        isMobile
                        color='black100'
                        title='What is Warp?'
                        button="About Warp"
                        textColor='white100'
                        description="“New era of travel” - common universal way to solve all travel issues in one place"
                        filled={false}
                        text='Ready-to-launch high-tech travel Ecosystem:'
                    />       
                    <Spacer width={'100%'} height={32} /> 
                    <InfoGrid list={INFO} isMobile/>
                    <Spacer width={'100%'} height={64} />
                    <FillBadge isMobile/>
                </> :
                <>
                    <MainBadge
                        color='black100'
                        title='Smart travel eco-system'
                        textColor='white100'
                        labels={LABELS}
                        withContact='Contact Warp'
                        text='Warp is a unique travel eco-system where USERS can plan and manage all travel needs in one place in a truly new digital way and PARTNERS can seamlessly deliver their travel content to millions of customers at the best price in all possible variations, creating new “phygital” environment in travel & lifestyle industries'
                        button="Connect"/>   
                    <Spacer width={'100%'} height={80} />      
                    <MainBadge
                        color='black200'
                        textColor='white100'
                        title='Warp Products'
                        text='Users & Partners benefit from their offers both in real and virtual worlds. They both get the same tools for planning travels and offering travel products using variety of Products built around Core platform benefiting from each other.'
                    />                 
                    <Products list={PRODUCTS} />    
                    <Spacer width={'100%'} height={80} />   
                    <MainBadge
                        color='black100'
                        title='What is Warp?'
                        button="About Warp"
                        textColor='white100'
                        description="“New era of travel” - common universal way to solve all travel issues in one place"
                        filled={false}
                        text='Ready-to-launch high-tech travel Ecosystem:'
                        />  
                    <Spacer width={'100%'} height={100} />
                    <InfoGrid list={INFO} />
                    <Spacer width={'100%'} height={100} />
                    <FillBadge />
                </>
            }
        </BaseLayout>
    )
}