import React from 'react';
import './App.css';
import {Route, Routes} from "react-router-dom";
import {ROUTES} from "./data/constants";
import {MainPage} from "./pages/MainPage";
import {ProductsPage} from "./pages/ProductsPage";
import {PartnersPage} from "./pages/PartnersPage";
import {InvestorsPage} from "./pages/InvestorsPage";
import {ContactPage} from "./pages/ContactPage";
import {AboutPage} from "./pages/AboutPage";
import {isMobile} from 'react-device-detect'
import { Policy } from './pages/documents/Policy';
import { Terms } from './pages/documents/Terms';

function App() {
  return (
     <Routes>
       <Route path={ROUTES.MAIN} element={<MainPage isMobile={isMobile}/>} />
       <Route path={ROUTES.ABOUT} element={<AboutPage isMobile={isMobile}/>} />
       <Route path={ROUTES.CONTACT} element={<ContactPage isMobile={isMobile}/>} />
       <Route path={ROUTES.INVESTORS} element={<InvestorsPage isMobile={isMobile}/>} />
       <Route path={ROUTES.PARTNERS} element={<PartnersPage isMobile={isMobile}/>} />
       <Route path={ROUTES.PRODUCTS} element={<ProductsPage isMobile={isMobile}/>} />
       <Route path={ROUTES.POLICY} element={<Policy isMobile={isMobile}/>} />
       <Route path={ROUTES.TERMS} element={<Terms isMobile={isMobile}/>} />
     </Routes>
  );
}

export default App;
