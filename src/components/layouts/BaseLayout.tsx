import React, { FC, ReactNode } from 'react'
import { StyledWrapper } from './styled'
import {theme} from "../../data/colors";

export type TBaseLayoutProps = {
  justifyContent?: 'flex-start' | 'center'
  backgroundColor?: keyof typeof theme['colors']
  children?: ReactNode,
  isMobile?: boolean
}

export const BaseLayout: FC<TBaseLayoutProps> = ({
  justifyContent,
  backgroundColor,
  children,
  isMobile
}) => {
  return (
    <StyledWrapper
      isMobile={isMobile}
      justifyContent={justifyContent}
      backgroundColor={backgroundColor}
    >
      {children}
    </StyledWrapper>
  )
}
