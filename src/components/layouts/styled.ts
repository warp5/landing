import styled from '@emotion/styled'
import {TBaseLayoutProps} from "./BaseLayout";
import {theme} from "../../data/colors";

export const StyledWrapper = styled('div')<TBaseLayoutProps & {isMobile?: boolean}>`
  display: flex;
  align-items: center;
  justify-content: ${({ justifyContent }) => justifyContent || 'center'};
  padding: ${({isMobile}) => isMobile ? `0 20px 24px` : `0`};
  margin-top: ${({isMobile}) => isMobile ? `100px` : `120px`};
  flex-direction: column;
  width: 100vw;
  min-height: 100vh;
  min-height: var(--app-height);
  overflow: hidden; 
  margin-bottom: 10vh;
  background-color: ${({ backgroundColor }) =>
    backgroundColor ? theme.colors[backgroundColor] : theme.colors.black100};
`
