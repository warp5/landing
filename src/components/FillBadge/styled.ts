import styled from "@emotion/styled";
import { theme } from "../../data/colors";

export const StyledWrapper = styled.div<{more?: boolean}>`
    display: flex;
    flex-direction: column;
    width: 100%;
    border-radius: 16px;
    background: ${({more}) => more ? theme.colors.white300 : theme.colors.lightPallet};
    justify-content: center;
    align-items: center;
    padding: 24px;
`