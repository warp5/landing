import {FC} from "react";
import {Spacer} from "../Spacer/Spacer";
import {StyledChevronRight, StyledConnectingButton, StyledImage, StyledImageWrapper, StyledInfoInnerWrapper, StyledInfoWrapper} from "../../pages/MainPage/styled";
import {Text} from "../Text/Text";
import { ROUTES } from "../../data/constants";
import { useNavigate } from "react-router-dom";
import { PageProps } from "../../data/types";
import { StyledWrapper } from "./styled";

export const FillBadge: FC<PageProps> = ({isMobile = false}) => {
    const history = useNavigate()
    const handleClick = () => {
        history(ROUTES.CONTACT)
    }
    return (
        <>            
            {isMobile ? 
            <StyledWrapper>
                <img src="/Default3.svg" alt="def" width={177} height={150}/>
                <Spacer width='100%' height={24} />
                <Text size='title24' colorTheme='black100' isBold align='center'>Connecting to the Warp platform for partnership</Text>
                <Spacer width='100%' height={8} />
                <Text size='text16' colorTheme='black100' align='center'>Fill out the brief and get the conditions for connecting to the platform</Text>                    
                <Spacer width='100%' height={16} />
                <StyledConnectingButton onClick={handleClick} isMobile>
                    <Text size='text16' colorTheme='white100' align='center'>Fill the brief</Text><StyledChevronRight/>
                </StyledConnectingButton>
            </StyledWrapper> 
            : 
            <StyledInfoWrapper bc='lightPallet' short>
                <StyledInfoInnerWrapper isStart>
                    <Text size='title40' colorTheme='black100' isBold align='left'>Connecting to the Warp platform for partnership</Text>
                    <Spacer width='100%' height={16} />
                    <Text size='text20' colorTheme='black100' align='left'>Fill out the brief and get the conditions for connecting to the platform</Text>                    
                    <Spacer width='100%' height={84} />
                    <StyledConnectingButton onClick={handleClick}>
                        <Text size='text20' colorTheme='white100' align='center'>Fill the brief</Text><StyledChevronRight/>
                    </StyledConnectingButton>
                </StyledInfoInnerWrapper>
                <StyledImageWrapper>
                    <StyledImage src={'/Default3.svg'} />
                </StyledImageWrapper>
            </StyledInfoWrapper>
            }
        </>
    )
}
