import { FC } from "react";
import { StyledInfoInnerWrapper, StyledInfoWrapper } from "../../pages/MainPage/styled";
import {Text} from "../../components/Text/Text";
import { theme } from "../../data/colors";
import { Spacer } from "../Spacer/Spacer";

export const InfoBadge: FC<{
    title: string,
    text?: string, 
    color?: keyof typeof theme['colors'],
    isMobile?: boolean,
    isTg?: boolean,
    isWu?: boolean,
    isEmail?: boolean
}> = ({title, text, color, isMobile, isTg = false, isWu = false, isEmail = false}) => {
    const handleTelegram = () => {
        window.open("https://t.me/Warp_Universe")
    }

    const handleWhatsup = () => {
        window.open("https://wa.me/message/I2LMELP4KYUDH1")
    }

    return (                    
            <StyledInfoWrapper bc={color}>                
                <StyledInfoInnerWrapper isMobile={isMobile} isTg={isTg}>
                    <Text size={isMobile ? 'title40' : 'title64'} colorTheme='white100' isBold align='center'>
                        {title}
                        {isTg ? <><Text size={isMobile ? 'title40' : 'title64'} colorTheme='link' as='span' onClick={handleTelegram}>Telegram</Text>{', '}</> : <></>}
                        {isWu ? <><Text size={isMobile ? 'title40' : 'title64'} colorTheme='link' as='span' onClick={handleWhatsup}>WhatsApp</Text>{' or '}</> : <></>}
                        {isEmail ? <Text size={isMobile ? 'title40' : 'title64'} colorTheme='link' as='span'>Email</Text> : <></>}
                    </Text>
                    {text ? 
                        <Text size={isMobile ? 'text16' : 'text20'} colorTheme='white100' align='center'>
                            {text}                            
                        </Text>
                         : <></>}
                    {isTg ? <Spacer width={'100%'} height={40}/> : <></>}
                </StyledInfoInnerWrapper>                                
            </StyledInfoWrapper>        
    )
}