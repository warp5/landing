import { FC } from "react";
import { generatePath, useNavigate } from "react-router-dom";
import { ROUTES, TProduct } from "../../data/constants";
import { scrollToTop } from "../../utils/utils";
import { Spacer } from "../Spacer/Spacer";
import { Text } from "../Text/Text";
import { StyledButton, StyledMobileProduct, StyledMobileWrapper, StyledSymbol, StyledTextWrapper, StyledWarpProduct, StyledWrapper } from "./styled";

export const Products: FC<{
    list: TProduct[],
    noFont?: boolean,
    isMobile?: boolean
}> = ({
    list,
    noFont = false,
    isMobile = false
}) => {
    const history = useNavigate()
    const handleClick = (type: number) => () => {
        const path = generatePath(ROUTES.PRODUCTS, { type: type })
        scrollToTop()
        history(path, {state: { type }})
    }
    return (
        <>
            {isMobile ? 
            <StyledMobileWrapper>
                {
                    list.map(product => 
                        <StyledMobileProduct 
                            key={product.point}
                            color={product.color} 
                            onClick={handleClick(product.type ?? 0)}                           
                        >
                            {product.absolute ?
                             <></> : 
                             <>
                                <Spacer width={'100%'} height={48} /> 
                                <StyledSymbol />
                                <Spacer width={'100%'} height={48} /> 
                             </>}
                            <StyledTextWrapper onClick={product.type ? handleClick(product.type) : undefined}>
                                <div style={{width: '70%'}}>
                                    <Text size="title24" colorTheme='white100' isBold align="left">{product.point}</Text>
                                </div>
                                <div style={{
                                    width: '30%'}}>
                                    <StyledButton />
                                </div>
                            </StyledTextWrapper>
                        </StyledMobileProduct>
                    )
                }
            </StyledMobileWrapper> 
            : 
            <StyledWrapper noFont={noFont}>        
                {
                    list.map(product => 
                        <StyledWarpProduct 
                            key={product.point}
                            color={product.color}
                            absolute={product.absolute}
                            marginRight={product.marginRight}
                            marginLeft={product.marginLeft}
                            marginTop={product.marginTop}
                            marginBottom={product.marginBottom}
                            noFont={noFont}
                        >
                            {product.absolute ? <></> : <StyledSymbol />}
                            <StyledTextWrapper onClick={handleClick(product.type ?? 0)}>
                                <Text size="title24" colorTheme={noFont ? 'black100' : "white100"} isBold align="left">{product.point}</Text>
                                {noFont ? <></> : <StyledButton />}
                            </StyledTextWrapper>
                        </StyledWarpProduct>
                    )
                }
            </StyledWrapper>
            }
        </>
    )
}