import styled from "@emotion/styled";
import { theme } from "../../data/colors";
import { Button, Symbol } from "../Icons";

export const StyledWrapper = styled.div<{noFont:boolean}>`
    display: flex;
    direction: column;
    width: 100%;
    height: 750px;
    ${({noFont}) => !noFont ? `
        background: ${theme.colors.black200};
        background-image: url('/bg.svg');
        background-repeat: no-repeat; 
        background-size: 1016px 614px; 
        background-position: center;` : `position: absolute;`
    }
    align-items: center;
    justify-content: center;
`
export const StyledWarpProduct = styled.div<{
    color: keyof typeof theme['colors'],
    absolute?: boolean,
    marginRight?: string,
    marginLeft?: string,
    marginTop?: string,
    marginBottom?: string,
    noFont?: boolean
}>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 32px;    
    ${({noFont}) => !noFont && `width: 288px`} ;
    gap: 40px;
    background: ${({color}) => color ? theme.colors[color] : theme.colors.primary100};
    border-radius: 24px;
    ${({absolute}) => absolute && `position: absolute;`}
    ${({absolute, noFont}) => (absolute && noFont) && `height: 60px;`}
    ${({absolute, noFont}) => (absolute && !noFont) && `height: 77px;`}
    ${({marginRight}) => marginRight && `margin-right: ${marginRight};`}
    ${({marginLeft}) => marginLeft && `margin-left: ${marginLeft};`}
    ${({marginTop}) => marginTop && `margin-top: ${marginTop};`}
    ${({marginBottom}) => marginBottom && `margin-bottom: ${marginBottom};`}
`

export const StyledTextWrapper = styled.div`
    display: flex;
    gap: 8px;
    width: 100%;
`

export const StyledButton = styled(Button)`
    margin-top: -4px;    
    margin-left: 16px;
    width: 44px;
    height: 44px;    
`

export const StyledSymbol = styled(Symbol)`
    width: 116px;
    height: 92px;
`

export const StyledMobileWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    background: ${theme.colors.black200};
    align-items: center;
    justify-content: center;
    gap: 12px;
`

export const StyledMobileProduct = styled.div<{
    color: keyof typeof theme['colors']
}>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 24px;    
    width: 100%;    
    background: ${({color}) => color ? theme.colors[color] : theme.colors.primary100};
    border-radius: 16px;    
`