import { FC } from "react";
import { TGrid, TProduct } from "../../data/constants";
import { StyledGrid, StyledRecordWrapper } from "./styled";
import {Text} from "../../components/Text/Text";
import { theme } from "../../data/colors";
import { Spacer } from "../Spacer/Spacer";

export const BadgeGrid: FC<{
    list?:TGrid[] | TProduct[],
    selected?: number, 
    setSelected?: (value: number) => void,
    background?: string,
    isMobile?: boolean
}> = ({list, selected, setSelected, background, isMobile}) => {    
    const hanldeClick = (value: number) => () => {
        if (setSelected) setSelected(value)
        if (list) {
            const val = list[value] as TGrid
            if (val.link1)
                window.open(val.link1)
        }
    }

    return (    
        <> 
        {isMobile ? 
        <div style={{
            width: '100%',
            background: theme.colors.black100,
            display: "flex",
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            gap: '12px'            
        }}>
            {list && list.length ? list.map((item, index) =>
                <StyledRecordWrapper 
                    key={index} 
                    isMobile
                    bc={index === selected ? 'black100' : 'white300'} 
                    onClick={hanldeClick(index)}
                >
                    {item.point ? 
                        <Text size='text16' colorTheme={index === selected ? 'white100' : 'black100'} isBold align='left'>{item.point}</Text> :
                        <>
                            <Text size='text16' colorTheme='black100' isBold align='left'>{item.point ? item.point : item.title}</Text>
                            {item.text ? <Text size='text12' colorTheme='black300' align='left'>{item.text}</Text> : <></>}                    
                        </>
                    }                        
                </StyledRecordWrapper>
            ) : <></>}
        </div> :
            <div style={{
                width: '100%',
                background: theme.colors.black100,
                display: "flex",
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'                
            }}>
                <Spacer width={'100%'} height={70} />
                <StyledGrid background={background}>
                    {list && list.length ? list.map((item, index) =>
                        <StyledRecordWrapper key={index} bc={index === selected ? 'black100' : 'white300'} onClick={hanldeClick(index)}>
                        {item.point ? 
                                <Text size='text20' colorTheme={index === selected ? 'white100' : 'black100'} isBold align='left'>{item.point}</Text> :
                            <>
                                <Text size='text20' colorTheme='black100' isBold align='left'>{item.point ? item.point : item.title}</Text>
                                {item.text ? <Text size='text16' colorTheme='black300' align='left'>{item.text}</Text> : <></>}                    
                            </>
                        }                        
                        </StyledRecordWrapper>
                    ) : <></>}
                </StyledGrid>
                <Spacer width={'100%'} height={70} />
            </div>
        }
        </>   
    )
}