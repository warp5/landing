import styled from "@emotion/styled"
import { Grid } from "@mui/material"
import { theme } from "../../data/colors"

export const StyledGrid = styled(Grid)<{background?: string}>`
  flex-direction: row;
  position: relative;
  place-items: left;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 30px;
  margin-bottom: auto;
  width: 70%;
  background: ${theme.colors.black100};
  ${({background}) => background && `background-image: url(${background}); background-repeat: no-repeat; background-size: cover;`}
`

export const StyledRecordWrapper = styled.div<{
  bc?: keyof typeof theme['colors'],
  isMobile?: boolean
}>`
    background: ${({ bc }) => bc ? theme.colors[bc] : theme.colors.white300};
    border-radius: ${({isMobile}) => isMobile ? `12px` : `50px`};
    width: ${({isMobile}) => isMobile ? `100%` : `80%`};
    padding: 40px;
    justify-content: center;
    align-items: center;
    display: flex;
    flex-direction: column;
`