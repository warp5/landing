import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Button } from './Button'

export default {
  title: 'Components/Button',
  component: Button
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof Button>

const Template: ComponentStory<typeof Button> = ({ children, ...args }) => (
  <Button {...args}>{children}</Button>
)
export const Primary = Template.bind({})
Primary.args = {
  color: 'primary',
  variant: 'contained',
  size: 'medium',
  children: 'Primary'
}

export const Secondary = Template.bind({})
Secondary.args = {
  variant: 'contained',
  color: 'secondary',
  size: 'medium',
  children: 'Secondary'
}

export const Info = Template.bind({})
Info.args = {
  variant: 'contained',
  color: 'info',
  size: 'medium',
  children: 'Info'
}

export const Loading = Template.bind({})
Loading.args = {
  color: 'primary',
  variant: 'contained',
  size: 'medium',
  children: 'Loading',
  isLoading: true
}
