import styled from "@emotion/styled"
import { theme } from "../../data/colors"
import { TimePoint } from "../Icons"


export const StyledInfoGrid = styled.div`
    display: grid;
    flex-direction: row;
    position: relative;
    width: 60%;
    align-items: center;
    justify-content: center;
    background-image: url('/tl.svg');
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
`
export const StyledRecord = styled.div<{position: string, zoom?: number}>`
    display: flex;
    width: ${({zoom}) => zoom ? (zoom > 90 ? `51.5%` : `51%`) : `51.5%`};
    margin-left: ${({position}) => position === `left` ? `0` : `auto`};
    margin-right: ${({position}) => position === `left` ? `auto` : `0`};
    margin-top: -35px;
`

export const StyledRight = styled.div`
    position: relative;   
    margin-bottom: 7px;
    display: block; 
    flex-direction: column;
    margin-left: auto;
    margin-right: 0;
    user-select: none;
    padding: 32px;
    width: 90%;
    background: ${theme.colors.black300};
    border-radius: 16px;
`

export const StyledLeft = styled.div`
    position: relative;  
    margin-bottom: 7px;
    display: block; 
    flex-direction: column;
    margin-left: 0;
    margin-right: auto;
    padding: 32px;
    width: 90%;
    background: ${theme.colors.black300};
    border-radius: 16px;
`

export const StyledNumber = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 41px;
    height: 36px;
    border-radius: 40px;
    background: ${theme.colors.primary100}
`

export const StyledTimePoint = styled(TimePoint)`
    margin-top: 30px;
`

export const StyledMobileWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    gap: 12px;
`

export const StyledMobileRecord = styled.div`    
    display: flex; 
    flex-direction: column;
    padding: 24px;
    width: 100%;
    background: ${theme.colors.black300};
    border-radius: 12px;
`

export const StyledMobileNumber = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 35px;
    height: 32px;
    border-radius: 40px;
    background: ${theme.colors.primary100}
`
