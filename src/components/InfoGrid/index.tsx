import { StyledInfoGrid, StyledLeft, StyledMobileNumber, StyledMobileRecord, StyledMobileWrapper, StyledNumber, StyledRecord, StyledRight, StyledTimePoint } from "./styled"
import { Text } from "../../components/Text/Text";
import { FC } from "react";
import { TInfo } from "../../data/constants";
import { Spacer } from "../Spacer/Spacer";

export const InfoGrid: FC<{list: TInfo[], isMobile?: boolean}> = ({list, isMobile}) => {    
    return (
        <>
            {isMobile ?
                <StyledMobileWrapper>
                    {list.map(item => 
                    <StyledMobileRecord key={item.title}>
                        <StyledMobileNumber>
                            <Text size="text14" colorTheme="white100" align="left" isBold>{item.title}</Text>
                        </StyledMobileNumber>
                        <Spacer width={'100%'} height={16}/>
                        <Text size="text16" colorTheme="white100" align="left" isBold>{item.text}</Text>
                    </StyledMobileRecord>)}
                </StyledMobileWrapper> :
                <StyledInfoGrid>
                    {list.map(item => <InfoRecord key={item.title} {...item}/>)}
                </StyledInfoGrid>
            }
        </>
    )
}

const InfoRecord: FC<TInfo> = ({title, text, position}) => {
    const zoom = (( window.outerWidth - 10 ) / window.innerWidth) * 100
    return (
        <>
            {position === 'left' 
            ? 
            <StyledRecord position={position} zoom={zoom}>
                <StyledLeft>
                    <StyledNumber>
                        <Text size="text16" colorTheme="white100" align="left" isBold>{title}</Text>
                    </StyledNumber>
                    <Spacer width={'100%'} height={24}/>
                    <Text size="text20" colorTheme="white100" align="left" isBold>{text}</Text>
                </StyledLeft> 
                {/* <StyledTimePoint/> */}
            </StyledRecord>
            : 
            <StyledRecord position={position} zoom={zoom}>
                {/* <StyledTimePoint/> */}
                <StyledRight>
                    <StyledNumber>
                        <Text size="text16" colorTheme="white100" align="left" isBold>{title}</Text>
                    </StyledNumber>
                    <Spacer width={'100%'} height={24}/>
                    <Text size="text20" colorTheme="white100" align="left" isBold>{text}</Text>                
                </StyledRight>
            </StyledRecord>
            }
        </>
    )
}
