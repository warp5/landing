import {FC, useState} from "react";
import {Text} from "../Text/Text";
import {TFAQ} from "../../data/constants";
import {StyledFAQWrapper, StyledMinus, StyledPlus, StyledWrapper} from "./styled";
import { PageProps } from "../../data/types";

export const FAQGrid: FC<PageProps & {list: TFAQ[]}> = ({isMobile, list}) => {
    return (        
            <StyledWrapper>
                <Text size='title64' colorTheme='black100' isBold align='left'>FAQ</Text>
                {list.map((item, index) => <FAQRecord title={item.title} index={item.index} answers={item.answers} key={index} isMobile={isMobile} list={list}/>)}
            </StyledWrapper>        
    )
}

const FAQRecord: FC<TFAQ & {isMobile?: boolean, list: TFAQ[]}> = ({index, title, answers, isMobile, list}) => {
    const [open, setOpen] = useState(false)
    const handleClick = () => {
        setOpen(prevState => !prevState)
    }
    return (
        <>
        {open && answers && answers.length && index === list.length-1 ? answers.map((item) => <FAQRecord key={item.index} index={item.index} title={item.title} isMobile={isMobile} list={list}/>) : <></>}
        <StyledFAQWrapper 
            last={index === list.length-1 && answers && answers.length > 0} 
            first={index === 0 && answers && answers.length > 0} 
            onClick={handleClick}
            isMobile={isMobile}
        >
            <Text 
                size={answers && answers.length ? 'text16' : 'text14'}
                colorTheme={answers && answers.length ? 'black200' : 'black500'} 
                align='left' 
                isBold 
                key={index}
            >
                {title}
            </Text>
            {answers && answers.length ? (open ? <StyledMinus /> : <StyledPlus />) : <></>}
        </StyledFAQWrapper>
        {open && answers && answers.length && index !== list.length-1 ? answers.map((item) => <FAQRecord key={item.index} index={item.index} title={item.title} isMobile={isMobile} list={list}/>) : <></>}
        </>
    )
}
