import styled from "@emotion/styled";
import {theme} from "../../data/colors";
import {MinusLogo, PlusLogo} from "../Icons";

export const StyledFAQWrapper = styled.div<{last?: boolean, first?: boolean, isMobile?: boolean}>`
    width: ${({isMobile}) => isMobile ? '100%' : '70vw'};
    border-top: 2px solid ${theme.colors.black500};
    border-right: 2px solid ${theme.colors.black500};
    border-left: 2px solid ${theme.colors.black500};
    display: flex;
    padding: ${({isMobile}) => isMobile ? `15px` : `10px 40px`};
    ${({ last }) => last ? `border-bottom: 2px solid ${theme.colors.black500};` : ``};
    ${({ last }) => last ? `border-radius: 0px 0px 20px 20px;` : ``};
    ${({ first }) => first ? `border-radius: 20px 20px 0px 0px;` : ``};
    background: ${theme.colors.white200}
`

export const StyledPlus = styled(PlusLogo)`
    margin-right: 0px;
    margin-left: auto;
`

export const StyledMinus = styled(MinusLogo)`
    margin-right: 0px;
    margin-left: auto;
`

export const StyledWrapper = styled.div`
    display: flex;
    flex-direction: column;     
`
