import { MouseEventHandler, ElementType, ReactNode, CSSProperties } from 'react'
import {theme} from "../../data/colors";

export type TTextProps = {
  size: 'text12'
   | 'text14' 
   | 'text16'
   | 'text20'
   | 'title24'
   | 'title32'
   | 'title40'
   | 'title64'
  colorTheme?: keyof typeof theme['colors']
  isBold?: boolean
  align?: 'left' | 'right' | 'center'
  opacity?: number
  className?: string
  href?: string
  as?: ElementType
  onClick?: MouseEventHandler
  style?: CSSProperties
  children?: ReactNode
}
