import styled from '@emotion/styled'
import { TTextProps } from './types'
import {theme} from "../../data/colors";

const fontSize = {
  text12: 12,
  text14: 14,
  text16: 16,
  text20: 20,
  title24: 24,
  title32: 32,
  title40: 40,
  title64: 64
}
const lineHeight = {
  text12: 16,
  text14: 20,
  text16: 22,
  text20: 26,
  title24: 32,
  title32: 40,
  title40: 48,
  title64: 72
}

const letterSpacing = {
  text12: 0.2,
  text14: 0.2,
  text16: 0.2,
  text20: 0.2,
  title24: 0.2,
  title32: -0.2,
  title40: -0.2,
  title64: -0.8
}

export const TextUI = styled('p')<TTextProps>`
  font-family: 'GT Walsheim Pro', sans-serif;
  font-weight: ${({ isBold }) => (isBold ? 500 : 400)};
  font-size: ${({ size }) => `${fontSize[size]}px`};
  line-height: ${({ size }) => `${lineHeight[size]}px`};
  letter-spacing: ${({ size }) => `${letterSpacing[size]}px`};
  color: ${({ colorTheme }) =>
    colorTheme ? theme.colors[colorTheme] : theme.colors.black100};
  text-align: ${({ align }) => align};
  opacity: ${({ opacity }) => opacity || 1};
`