import styled from "@emotion/styled";
import { theme } from "../../data/colors";
import { ArrowIcon } from "../Icons";

export const StyledButton = styled.div`
   position: fixed; 
   width: 100%;
   left: 95%;
   bottom: 40px;
   height: 20px;
   font-size: 3rem;
   z-index: 1;
   cursor: pointer;
   z-index: 20;
   color: ${theme.colors.black100};
`

export const StyledArrow = styled(ArrowIcon)<{visible: boolean}>`
    ${({visible}) => visible ? `display: inline` : `display: none`}    
`