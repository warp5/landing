import { FC, useState } from "react";
import { StyledArrow, StyledButton } from "./styled";

export const ScrollButton: FC = () => {
    const [visible, setVisible] = useState(false)
    const toggleVisible = () => {
        const scrolled = document.documentElement.scrollTop;
        if (scrolled > 300){
          setVisible(true)
        } 
        else if (scrolled <= 300){
          setVisible(false)
        }
      }

    const scrollToTop = () =>{
        window.scrollTo({
          top: 0, 
          behavior: 'smooth'
        })
    }

    window.addEventListener('scroll', toggleVisible)

    return (
        <StyledButton onClick={scrollToTop}>        
            <StyledArrow visible={visible} />
        </StyledButton>
    )
}