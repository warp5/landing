import React, { ChangeEvent, useState } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { BaseField } from './BaseField'

export default {
  title: 'Fields/BaseField',
  component: BaseField
} as ComponentMeta<typeof BaseField>

const args = {
  name: 'login',
  label: 'Электронная почта или логин'
}

const Template: ComponentStory<typeof BaseField> = args => (
  <BaseField {...args} />
)

export const Base = Template.bind({})
Base.args = {
  ...args
}

export const TextArea = Template.bind({})
TextArea.args = {
  ...args,
  multiline: true,
  rows: 3,
  value: 'Textarea высотой 3 строки',
  onClearField: undefined
}

export const Disabled = Template.bind({})
Disabled.args = {
  ...args,
  disabled: true
}

export const WithHelperText = Template.bind({})
WithHelperText.args = {
  ...args,
  helperText: 'Дополнительный текст к инпуту'
}

export const WithErrorText = Template.bind({})
WithErrorText.args = {
  ...args,
  helperText: 'Ошибочка :)',
  error: true
}

const TemplateClearField: ComponentStory<typeof BaseField> = ({
  children,
  ...args
}) => {
  const [value, setValue] = useState('Нажми на крестик')
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }

  const handleClearField = () => {
    setValue('')
  }

  return (
    <BaseField
      {...args}
      value={value}
      onChange={handleChange}
      onClearField={handleClearField}
    >
      {children}
    </BaseField>
  )
}

export const ClearField = TemplateClearField.bind({})
ClearField.args = { ...Base.args }
