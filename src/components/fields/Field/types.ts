import { TextFieldProps } from '@mui/material'

export type TFieldProps = TextFieldProps & { isMobile?: boolean, onClearField?: () => void }
