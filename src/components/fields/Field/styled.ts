import styled from '@emotion/styled'
import { TextField } from '@mui/material'
import { theme } from '../../../data/colors'

export const StyledField = styled(TextField)<{noBorder?: boolean, isMobile?: boolean}>`
  & .MuiInputLabel-root {
    font-family: 'GT Walsheim Pro', sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: ${({isMobile}) => isMobile ? `12px` : `16px`};
    line-height: 20px;
    letter-spacing: -0.2px;
    &.Mui-focused {
      color: ${theme.colors.black500};
    }
    &.Mui-error {
      color: ${theme.colors.error};
    }
  }
  &.MuiTextField-root {
    border-radius: 16px;
    &:focus,
    &:focus-within,
    &:focus-visible {
    }
  }
  & .MuiFilledInput-root {
    background: ${theme.colors.white100};
    border: ${({noBorder}) => noBorder ? '0' : '5px'} solid ${theme.colors.white300};
    border-radius: 16px;
    &.MuiInputBase-adornedEnd {
      padding-right: 16px;
    }
    &.Mui-focused,
    &.Mui-disabled {
      background: ${theme.colors.white100};
    }
    &:hover {
      background-color: ${theme.colors.white100};
    }
  }
  & .MuiFilledInput-input {
    font-family: 'GT Walsheim Pro', sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: ${({isMobile}) => isMobile ? `12px` : `16px`};
    line-height: 20px;
    letter-spacing: -0.2px;
    color: ${theme.colors.black200};
    background: ${theme.colors.white100};
    border-radius: 16px;
  }
  & .MuiFormHelperText-root {
    font-family: 'GT Walsheim Pro', sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: ${({isMobile}) => isMobile ? `12px` : `16px`};
    line-height: 20px;
    letter-spacing: -0.1px;
    color: ${theme.colors.black500};
    &.Mui-error {
      color: ${theme.colors.error};
    }
  }
`
