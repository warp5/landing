import styled from "@emotion/styled";
import {theme} from "../../data/colors";
import { Point } from "../Icons";

export const StyledFooter = styled.div<{isMobile?: boolean}>`  
  width: 100%;
  height: ${({isMobile}) => isMobile ? `405px` : `200px`};
  position: relative;
  bottom: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  z-index: 10;
  padding: ${({isMobile}) => isMobile ? `40px 20px` : `40px 108px`};
  background-color: ${theme.colors.black200};
  border-top: 1px solid ${theme.colors.black400};
  gap: 30px;
`

export const StyledInnerFooter = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  position: relative;
  height: 100%;
  gap: 24px;
`

export const StyledOuterFooter = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  height: 100%;
  gap: 24px;
  justify-content: center;
  align-items: center;
`

export const StyledDocsWrapper = styled.div<{isMobile?: boolean}>`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: ${({isMobile}) => isMobile ? `100%` : `80%`};
`

export const StyledLowerWrapper = styled.div<{right?: boolean, isMobile?: boolean}>`
  display: flex;
  width: ${({isMobile}) => isMobile ? `100%` : `30%`};
  gap: 24px;
  ${({right}) => right ? `margin-right: 0; margin-left: auto;` : ``}
`

export const StyledPoint = styled(Point)`
  margin-left: 5px;
  margin-right: 5px;
  margin-bottom: 5px;
`