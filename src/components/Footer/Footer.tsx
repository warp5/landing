import {FC} from "react";
import { StyledText } from "../../pages/MainPage/styled";
import { Spacer } from "../Spacer/Spacer";
import {Text} from "../Text/Text";
import { Divider } from '@mui/material'
import {StyledDocsWrapper, StyledFooter, StyledInnerFooter, StyledLowerWrapper, StyledOuterFooter, StyledPoint} from "./styled";
import { theme } from "../../data/colors";
import { PageProps } from "../../data/types";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../data/constants";
import { scrollToTop } from "../../utils/utils";

export const Footer: FC<PageProps> = ({isMobile}) => {
    const history = useNavigate()
    const handleTerms = () => {
        scrollToTop()
        history(ROUTES.TERMS)
    }
    const handlePolicy = () => {
        scrollToTop()
        history(ROUTES.POLICY)
    }

    const handleTelegram = () => {
        window.open("https://t.me/Warp_Universe")
    }

    const handleWhatsup = () => {
        window.open("https://wa.me/message/I2LMELP4KYUDH1")
    }

    return (
        <StyledFooter isMobile={isMobile}>
            {isMobile ?
                <>
                    <StyledDocsWrapper isMobile>
                        <StyledText size='text14' colorTheme='white100' isBold align="left">Phone number</StyledText>
                        <Spacer width='100%' height={4} />
                        <Text size='title24' colorTheme='white100' isBold align="left">+371 283 54789</Text>
                    </StyledDocsWrapper>
                    <StyledDocsWrapper isMobile>
                        <StyledText size='text14' colorTheme='white100' isBold align="left">Contact email</StyledText>
                        <Spacer width='100%' height={4} />
                        <Text size='title24' colorTheme='white100' isBold align="left">info@warp.com</Text>
                    </StyledDocsWrapper>
                    <StyledDocsWrapper isMobile>
                        <StyledText size='text14' colorTheme='white100' isBold align="left">Messengers</StyledText>
                        <Spacer width='100%' height={4} />
                        <Text size='title24' colorTheme='white100' isBold align="left">
                            <Text size='title24' colorTheme='white100' isBold align="left" as="span" onClick={handleTelegram}>Telegram</Text>
                        <StyledPoint/>
                            <Text size='title24' colorTheme='white100' isBold align="left" as="span" onClick={handleWhatsup}>WhatsApp</Text>
                        </Text>
                    </StyledDocsWrapper>
                    <Divider style={{width: '130%', background: theme.colors.black400}}/>
                    <StyledDocsWrapper isMobile>
                        <StyledText size='text16' colorTheme='white100' isBold align="left">Copyright © 2023 Warp All rights reserved</StyledText>
                        <Spacer width='100%' height={16} />
                        <StyledLowerWrapper isMobile>
                            <Text size='text16' colorTheme='primary100' onClick={handlePolicy}>Privacy Policy</Text>
                            <Text size='text16' colorTheme='primary100' onClick={handleTerms}>Terms of Use</Text>
                        </StyledLowerWrapper>
                    </StyledDocsWrapper>
                </> :
                <>
                    <StyledInnerFooter>
                        <StyledDocsWrapper>
                            <StyledText size='text16' colorTheme='white100' isBold align="left">Phone number</StyledText>
                            <Spacer width='100%' height={8} />
                            <Text size='title32' colorTheme='white100' isBold align="left">+371 283 54789</Text>
                        </StyledDocsWrapper>
                        <StyledDocsWrapper>
                            <StyledText size='text16' colorTheme='white100' isBold align="left">Contact email</StyledText>
                            <Spacer width='100%' height={8} />
                            <Text size='title32' colorTheme='white100' isBold align="left">info@warp.com</Text>
                        </StyledDocsWrapper>
                        <StyledDocsWrapper>
                            <StyledText size='text16' colorTheme='white100' isBold align="left">Messengers</StyledText>
                            <Spacer width='100%' height={8} />
                            <Text size='title32' colorTheme='white100' isBold align="left">
                                <Text size='title32' colorTheme='white100' isBold align="left" as="span" onClick={handleTelegram}>Telegram</Text>
                                <StyledPoint/>
                                <Text size='title32' colorTheme='white100' isBold align="left" as="span" onClick={handleWhatsup}>WhatsApp</Text>
                            </Text>                            
                        </StyledDocsWrapper>
                    </StyledInnerFooter>  
                    <Divider style={{width: '130%', background: theme.colors.black400}}/>         
                    <StyledOuterFooter>
                        <StyledLowerWrapper>
                            <StyledText size='text16' colorTheme='white100'>Copyright © 2023 Warp All rights reserved</StyledText>
                        </StyledLowerWrapper>
                        <StyledLowerWrapper right>
                            <Text size='text16' colorTheme='primary100' onClick={handlePolicy}>Privacy Policy</Text>
                            <Text size='text16' colorTheme='primary100' onClick={handleTerms}>Terms of Use</Text>
                        </StyledLowerWrapper>
                    </StyledOuterFooter>  
                </>
            }                   
        </StyledFooter>
    )
}