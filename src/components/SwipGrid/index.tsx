import { FC, ReactNode, useState } from "react"
import { TSwipGrid } from "../../data/constants"
import { StyledInfoWrapper, StyledLeftArrow, StyledRightArrow, StyledWrapper } from "./styled"
import { Swiper, SwiperProps, SwiperSlide, useSwiper } from 'swiper/react'
import { Navigation, Pagination } from 'swiper'
import {Text} from "../Text/Text"
import "swiper/css";
import "swiper/css/pagination";
import { RoundLogo } from "../Icons"
import { InfoBadge } from "../InfoBadge"
import { Spacer } from "../Spacer/Spacer"

export const SwipGrid: FC<{list: TSwipGrid[], isMobile?: boolean}> = ({list, isMobile}) => {
    const [index, setIndex] = useState(0)
    const swiperOnTransitionEnd: SwiperProps['onTransitionEnd'] = swiper => {
        setIndex(swiper.activeIndex)
    }
    return (
        <StyledWrapper isMobile={isMobile}>
            <InfoBadge title="Success story" isMobile={isMobile}/>
            <Swiper
                direction='horizontal'
                slidesPerView={1}
                zoom={false}
                width={window.screen.width}
                modules={[Pagination, Navigation]}
                onTransitionEnd={swiperOnTransitionEnd}>
            {
                list.map((item, index) => (                
                    <SwiperSlide
                        zoom={false}
                        key={item.position}
                        virtualIndex={index}>
                        <SwipRecord {...item} isMobile={isMobile}/>
                    </SwiperSlide>
                    )
                )
            }

            {isMobile ? <></> :
            <div style={{
                display: 'flex',
                width: '75vw',
                position: 'absolute',
                marginTop: '-70px'
            }}>
                {index === 0 ? 
                <></> : 
                <SwipButton direction={0}>
                    <StyledLeftArrow />
                </SwipButton>}         
                {index === list.length-1 ? <></> :   
                <SwipButton direction={1}>
                    <StyledRightArrow />
                </SwipButton>
                }
            </div>
            }
            </Swiper>
            <div style={{
                 display: 'flex',
                 gap: '10px',
                 alignItems: 'center',
                 justifyContent: 'center'
            }}>
                {Array.from(Array(list.length)).map((_, i) => (
                    <RoundLogo key={i} selected={index === i}/>
                ))}
            </div>
        </StyledWrapper>
    )
}

const SwipRecord: FC<TSwipGrid & {isMobile?: boolean}> = ({text, author, isMobile}) => {
    return (
        <StyledInfoWrapper isMobile={isMobile}>
            <Spacer width={'100%'} height={isMobile ? 24 : 4} />
            <Text size={isMobile ? 'text16' : 'text20'} colorTheme='white100' isBold align='center'>{'“ '}{text}{' „'}</Text>
            <Spacer width={'100%'} height={isMobile ? 14 : 4} />
            <Text size={isMobile ? 'text12' : 'text16'} colorTheme="white100" align="left" isBold>{author}</Text>
            <Spacer width={'100%'} height={isMobile ? 24 : 4} />
        </StyledInfoWrapper>
    )
}

const SwipButton: FC<{
    direction: number,
    children?: ReactNode
}> = ({ direction, children }) => {
    const swiper = useSwiper();
    return (
        <div 
            style={{                
                height: '100%',
                zIndex: '10',
                marginLeft: direction === 0 ? '0' : 'auto',
                marginRight: direction === 0 ? 'auto' : '0'
            }}
            onClick={() => {
            if (direction === 1)
              swiper.slideNext()
            else
              swiper.slidePrev()
        }}>
            {children}
        </div>
  )
}