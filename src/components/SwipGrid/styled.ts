import styled from "@emotion/styled";
import { LeftArrow, RightArrow } from "../Icons";

export const StyledWrapper = styled.div<{isMobile?: boolean}>`  
  width: ${({isMobile}) => isMobile ? `100%` : `70%`};
  height: 100%;
  display: flex;
  flex-direction: column;  
  justify-content: center;
  align-items: center;  
`

export const StyledInfoWrapper = styled.div<{isMobile?: boolean}>`
    display: flex;
    flex-direction: column;   
    ${({isMobile}) => !isMobile && `padding: 30px 80px 50px;`}
    width: ${({isMobile}) => isMobile ? `90%` : `60%`};
    justify-content: center;
    ${({isMobile}) => isMobile && `align-items: center;`}
`

export const StyledLeftArrow = styled(LeftArrow)`
  margin-left: 0;
  margin-right: auto;
`

export const StyledRightArrow = styled(RightArrow)`
  margin-left: auto;
  margin-right: 0;
`