import {FC, useState} from "react";
import {StyledMainLogo, StyledWrapper} from "./styled";
import {ROUTES} from "../../data/constants";
import {useNavigate} from "react-router-dom";
import './styles.css'
import { PageProps } from "../../data/types";
import { DesktopMenu } from "./DesktopMenu";
import { MobileMenu } from "./MobileMenu";

export const HeaderMenu: FC<PageProps> = ({isMobile}) => {
    const [selected, setSelected] = useState(-1)
    const history = useNavigate()
    const handleMain = () => {
        setSelected(-1)
        history(ROUTES.MAIN)
    }
    return (
        <StyledWrapper>
            <div onClick={handleMain}>
                <StyledMainLogo isMobile={isMobile}/>
            </div>
            {isMobile ? 
                <MobileMenu /> :
                <DesktopMenu selected={selected} setSelected={setSelected}/>}
        </StyledWrapper>
    )
}