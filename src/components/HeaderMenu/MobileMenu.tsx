import { Divider } from "@mui/material";
import { FC, useEffect, useState } from "react";
import { generatePath, useNavigate } from "react-router-dom";
import { theme } from "../../data/colors";
import { MENU, ROUTES } from "../../data/constants";
import { StyledText } from "../../pages/MainPage/styled";
import { scrollToTop } from "../../utils/utils";
import { Spacer } from "../Spacer/Spacer";
import {Text} from "../Text/Text";
import { StyledBurger, StyledChevronOpen, StyledChevronRight, StyledCloseIcon, StyledMobileContactButton, StyledSwipeableDrawer } from "./styled";

export const MobileMenu: FC = () => {
    const history = useNavigate()
    const [open, setOpen] = useState(false)
    const [showSubPage, setShowSubPage] = useState(false)
    const handleOpenMenu = () => {
        setOpen(true)
    }
    const handleCloseMenu = () => {
        setOpen(false)
    }
    const handlePage = (page: string, index: number) => () => {
        if (page === ROUTES.PRODUCTS) {
            setShowSubPage(prevState => !prevState)
        } else {
            setOpen(false)
            history(page)
        }
    }
    const handleContact = () => {  
        setShowSubPage(false)
        setOpen(false)      
        history(ROUTES.CONTACT)
    }
    const handleClickSubMenu = (type: number) => () => {
        const path = generatePath(ROUTES.PRODUCTS, { type: type })
        scrollToTop()
        setShowSubPage(false)
        setOpen(false)
        history(path, {state: { type }})
    }

    useEffect(() => {
        setShowSubPage(false)
    }, [open])

    return (
        <>
            <div 
            onClick={handleOpenMenu}
            style={{
                display: 'flex',
                marginLeft: 'auto',
                marginRight: '0',
                gap: '12px',
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text
                    size='text16'
                    colorTheme='white100'
                    isBold                             
                >
                    {open ? 'Close' : 'Menu'}
                </Text>
                {open ? <StyledCloseIcon /> : <StyledBurger />}
            </div>
            <StyledSwipeableDrawer
                anchor='bottom'
                onOpen={handleOpenMenu}
                onClose={handleCloseMenu}
                open={open}
                keepMounted={false}
                disableSwipeToOpen
                BackdropProps={{
                    invisible: true
                }}
            >
                {MENU.map((item, index) => 
                    <div onClick={handlePage(item.route, index)}
                         key={index}
                         style={{
                            display: 'flex',
                            flexDirection: 'column',                            
                            width: '100%',                
                        }}
                        >
                        <div style={{
                                display: 'flex',
                                width: '100%', 
                                padding: '16px 0px'               
                        }}>
                            <Text
                                size='text16'
                                colorTheme={'white100'}                            
                                isBold                                                  
                            >
                                {item.title}
                            </Text>  
                            {showSubPage && item.dropDown && item.dropDown.length ? <StyledChevronOpen /> : <StyledChevronRight />}
                        </div>
                        {showSubPage && item.dropDown && item.dropDown.length ?
                            item.dropDown.map((d, i) => 
                                <div key={i}
                                    onClick={handleClickSubMenu(d.type ?? 0)}
                                    style={{
                                    display: 'flex',
                                    width: '90%', 
                                    padding: '16px 0px',
                                    marginLeft: '10px'              
                                }}>
                                <StyledText
                                    size='text16'
                                    colorTheme={'white100'}                            
                                    isBold                                                 
                                >
                                    {d.title}
                                </StyledText>                                  
                            </div>    
                            )
                             :
                            <></>
                        }
                        <Spacer width='100%' height={4} />
                        <Divider style={{width: '130%', background: theme.colors.black400}}/>
                    </div>    
                )}
                <StyledMobileContactButton onClick={handleContact}>
                    <Text size='text16' colorTheme='white100' isBold>Contact Warp</Text>
                </StyledMobileContactButton>
            </StyledSwipeableDrawer>    
        </>
    )
}