import { Dispatch, SetStateAction } from "react"

export type TMenuProps = {
    selected: number,
    setSelected: Dispatch<SetStateAction<number>>
}