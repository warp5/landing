import { FC, FocusEvent, useState } from "react";
import { generatePath, useNavigate } from "react-router-dom";
import { MENU, ROUTES, TMenu } from "../../data/constants";
import { StyledText } from "../../pages/MainPage/styled";
import { scrollToTop } from "../../utils/utils";
import { StyledChevronDown, StyledChevronUp, StyledContactButton, StyledTextMenu } from "./styled";
import {Text} from "../Text/Text";
import { TMenuProps } from "./types";

export const DesktopMenu: FC<TMenuProps> = ({
    selected,
    setSelected
}) => {
    const history = useNavigate()
    const handleSelected = (index: number) => () => {
        setSelected(index)
    }
    const handlePage = (page: string, index: number) => () => {
        setSelected(index)
        if (page === ROUTES.PRODUCTS) {
            const type = 0
            const path = generatePath(ROUTES.PRODUCTS, { type: type })
            history(path, {state: { type }})
        } else {
            history(page)
        }
    }
    const handleContact = () => {
        setSelected(-1)
        history(ROUTES.CONTACT)
    }
    return (
        <>
            <StyledTextMenu>
                {MENU.map((item, index) =>
                    (item.dropDown && item.dropDown.length ? 
                        <DropDownMenu 
                            list={item.dropDown} 
                            title={item.title}
                            selected={selected}
                            key={index}
                            index={index}
                            setSelected={handleSelected(index)}
                        /> :
                        <StyledText
                            size='text16'
                            colorTheme={selected === index ? 'white500' : 'white100'}
                            key={index}
                            isBold
                            onClick={handlePage(item.route, index)}                    
                        >
                            {item.title}
                        </StyledText>
                    )
                )}
            </StyledTextMenu>

            <StyledContactButton onClick={handleContact}>
                <Text size='text16' colorTheme='white100' isBold>Contact Warp</Text>
            </StyledContactButton>
        </>
    )
}


type DropDownProps = {
    list: TMenu[],
    showDropDown: boolean,
    toggleDropDown: () => void,
    setSelected: () => void
  }

const DropDownMenu: FC<{list: TMenu[], title: string, selected: number, setSelected: () => void, index: number}> = ({list, title, selected, setSelected, index}) => {
    const [showDropDown, setShowDropDown] = useState(false)
    
    const toggleDropDown = () => {
        setShowDropDown(!showDropDown);
    }

    const dismissHandler = (event: React.FocusEvent<HTMLButtonElement>): void => {
        if (event.currentTarget === event.target) {
          setShowDropDown(false)
        }
      }

    return (
        <button
            className={showDropDown ? "active" : undefined}
            onClick={(): void => toggleDropDown()}
            onBlur={(e: FocusEvent<HTMLButtonElement>): void => dismissHandler(e)}
      >
        <StyledText
            size='text16'
            colorTheme={selected === index ? 'white500' : 'white100'}            
            isBold                             
        >
            {title}{' '}{showDropDown ? <StyledChevronUp/> : <StyledChevronDown selected={selected === index}/>}
        </StyledText>
        {showDropDown && (
          <DropDown
            list={list}
            showDropDown={showDropDown}
            setSelected={setSelected}
            toggleDropDown={(): void => toggleDropDown()}            
          />
        )}
      </button>
    )
}

const DropDown: FC<DropDownProps> = ({list, showDropDown, setSelected}) => {
    const history = useNavigate()
    const onClickHandler = (type: number) => () => {
        const path = generatePath(ROUTES.PRODUCTS, { type: type })
        scrollToTop()
        setSelected()
        history(path, {state: { type }})
    };

    return (
        <div className={showDropDown ? 'dropdown' : 'dropdown active'}>
            {list.map((item, index) => {
                    return (
                    <StyledText
                        size='text16'
                        colorTheme={'white100'}          
                        isBold    
                        onClick={onClickHandler(item.type ?? 0)}
                    >
                        {item.title}
                    </StyledText>
                    )
                }
            )}
        </div>
    )
}
