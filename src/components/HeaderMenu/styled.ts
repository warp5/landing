import styled from "@emotion/styled";
import {Burger, ChevronDown, ChevronRight, ChevronUp, CloseIcon, MainLogo} from "../Icons";
import {theme} from "../../data/colors";
import {
    SwipeableDrawer,
    SwipeableDrawerProps
  } from '@mui/material'

export const StyledWrapper = styled.div`
    display: flex;
    top: 0;
    width: 100%;
    position: absolute;
    background: ${theme.colors.black200};
    padding: 18px;
    border-bottom: 1px solid ${theme.colors.black400};
    z-index: 10;
`

export const StyledTextMenu = styled.div`
    display: flex;
    gap: 30px;
    margin-left: 50px;
    align-items: center;
`

export const StyledContactButton = styled.div`
    display: flex;
    align-items: center;
    border-radius: 16px;
    border: 2px solid ${theme.colors.black500};
    height: 44px;
    width: 143px;
    justify-content: center;
    margin-right: 11vw;
    margin-left: auto;
    margin-top: -6px;
`

export const StyledMobileContactButton = styled.div`
    display: flex;
    align-items: center;
    border-radius: 10px;
    background: ${theme.colors.black400};
    height: 44px;
    width: 90%;
    justify-content: center;
    bottom: 40px;
    position: fixed;
`

export const StyledMainLogo = styled(MainLogo)<{isMobile?: boolean}>`
    margin-left: ${({isMobile}) => isMobile ? `0;` : `9vw;`}
    width: 83px;
    height: 34px;
`

export const StyledChevronUp = styled(ChevronUp)`
    margin-bottom: -8px;
    margin-right: -20px;
`
export const StyledChevronDown = styled(ChevronDown)`
    margin-bottom: -8px;
    margin-right: -20px;
`

export const StyledBurger = styled(Burger)`

`
export const StyledSwipeableDrawer = styled(
    SwipeableDrawer
  )<SwipeableDrawerProps>`
    & .MuiDrawer-paper {
      width: 100%;
      padding: 0 20px;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      height: 91%;
      background: ${theme.colors.black100}
    }
  `

  export const StyledChevronRight = styled(ChevronRight)`
    margin-left: auto;
    margin-right: 0;
  `

  export const StyledChevronOpen = styled(ChevronDown)`
    margin-left: auto;
    margin-right: 0;
  `

  export const StyledCloseIcon = styled(CloseIcon)`
    
  `
