import {FC} from "react";
import { theme } from "../../data/colors";

export type TLogoProps = {
    className?: string
}

export const MainLogo: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="84"
            height="32"
            viewBox="0 0 84 32"
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M27.7048 0C24.898 3.78648 23.0163 8.85172 21.7706 13.2318C19.8152 3.02865 17.965 0.681109 15.6303 0.681109C13.2957 0.681109 11.4127 3.06095 9.33726 13.4768C8.09283 9.03479 6.19891 3.85648 3.33347 0L0 2.40542C5.30107 9.55572 6.99033 21.3688 7.3683 24.5845H11.6774C12.7089 16.387 14.3436 8.68212 15.6235 5.70597C16.872 8.69154 18.4289 16.3964 19.3772 24.5845H23.6768C24.0493 21.3688 25.7385 9.55437 31.045 2.40542L27.7048 0Z" 
                fill="#6952FF"/>
            <path 
                d="M81.5559 8.29176C80.0004 6.98877 77.8363 6.73571 75.4771 7.61873C73.3428 8.39305 71.4509 9.70441 69.989 11.4227V7.48277H66.0688V32H69.9918V25.0005C70.8446 25.1781 71.9812 25.1351 73.5845 24.6182C77.9591 23.2183 82.5042 18.1773 83.3393 13.9129C83.7841 11.6246 83.1524 9.62571 81.5559 8.29176ZM79.486 13.1807C78.9251 16.0478 75.3188 19.9675 72.3401 20.9502C71.4955 21.2274 70.8391 21.2126 70.4571 20.9232C69.1581 19.3618 70.7736 16.1837 72.9691 13.9358C75.2301 11.6219 77.9987 10.3876 79.0179 11.2437C79.2172 11.4079 79.7466 11.8508 79.486 13.1807Z" 
                fill="#6952FF"/>
            <path 
                d="M43.3621 7.98082C41.7029 6.86224 39.4624 6.83532 37.0527 7.90544C35.1697 8.74135 33.258 10.1937 31.667 12.0042C27.9583 16.2187 27.2638 20.7779 29.8932 23.6194L30.0296 23.754C30.9714 24.5865 32.1984 25.0354 33.4627 25.0099C36.526 25.0099 41.3959 22.8131 43.4904 17.7559H43.6364C43.8806 19.8356 44.0348 22.3945 44.0621 24.5872H47.9946C47.9946 23.1644 47.9346 21.3311 47.7599 19.4035C47.2046 13.3395 45.7664 9.60417 43.3621 7.98082ZM38.9835 17.8071C36.6802 20.304 33.7984 21.7362 32.7231 20.9353C31.4801 19.4479 32.8228 16.597 34.6334 14.5456C37.0895 11.7579 40.0983 10.4683 41.1517 11.1804C41.2335 11.2362 41.3115 11.2973 41.385 11.3635V11.3716C42.4397 12.3273 41.3713 15.2146 38.9835 17.8017V17.8071Z"
                fill="#6952FF"/>
            <path 
                d="M62.0395 6.82455L55.0056 11.8467V7.48278H51.084V24.5845H55.0056V16.6239L64.3387 9.95685L62.0395 6.82455Z"
                fill="#6952FF"/>
        </svg>
    )
}

export const PlusLogo: FC<TLogoProps> = ({className}) => {
    return (
        <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M0 12H24"
                stroke={theme.colors.black200}
                strokeWidth="2"/>
            <path
                d="M12 24L12 -1.78814e-07"
                stroke={theme.colors.black200}
                strokeWidth="2"/>
        </svg>
    )
}

export const MinusLogo: FC<TLogoProps> = ({className}) => {
    return (
        <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M0 12H24"
                stroke={theme.colors.black200}
                strokeWidth="2"/>
        </svg>
    )
}

export const DefaultLogo: FC<TLogoProps> = ({ className }) => {
    return (
        <svg 
            width="84"
            height="84" 
            viewBox="0 0 84 84" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M2 2L81.9958 81.9958M82 2L2.00425 81.9958" 
                stroke="#CDCDCD" 
                strokeWidth="4"/>
        </svg>
    )
}

export const Cross: FC<TLogoProps> = ({ className }) => {
    return (
      <svg
        width='24'
        height='24'
        viewBox='0 0 24 24'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
        className={className}
      >
        <path
          d='M17.7071 7.70711C18.0976 7.31658 18.0976 6.68342 17.7071 6.29289C17.3166 5.90237 16.6834 5.90237 16.2929 6.29289L12 10.5858L7.70711 6.29289C7.31658 5.90237 6.68342 5.90237 6.29289 6.29289C5.90237 6.68342 5.90237 7.31658 6.29289 7.70711L10.5858 12L6.29289 16.2929C5.90237 16.6834 5.90237 17.3166 6.29289 17.7071C6.68342 18.0976 7.31658 18.0976 7.70711 17.7071L12 13.4142L16.2929 17.7071C16.6834 18.0976 17.3166 18.0976 17.7071 17.7071C18.0976 17.3166 18.0976 16.6834 17.7071 16.2929L13.4142 12L17.7071 7.70711Z'
          fill={'#707070'}
        />
      </svg>
    )
  }

  export const ArrowIcon: FC<TLogoProps> = ({className}) => {
    return (
        <svg
            width="44" 
            height="44" 
            viewBox="0 0 44 44" 
            fill="none"
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M27 24L22 19L17 24" 
                stroke="white" 
                strokeWidth="2" 
                strokeLinecap="round"/>
            <rect 
                x="1" 
                y="1" 
                width="42" 
                height="42" 
                rx="9" 
                stroke="#D0D0DB" 
                strokeWidth="2"/>
        </svg>
    )
  }

  export const RoundLogo: FC<{className?: string, selected: boolean}> = ({className, selected}) => {
    return (         
        <svg 
            width="16"
            height="16"
            viewBox="0 0 16 16"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <circle 
                cx="8" 
                cy="8" 
                r="8" 
                fill={selected ? theme.colors.black500 : theme.colors.white300}/>
        </svg>
    )
  }

  export const LeftArrow: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="29"
            height="50" 
            viewBox="0 0 29 50" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M27 2L3 25L27 48" 
                stroke="#CDCDCD" 
                strokeWidth="3" 
                strokeLinecap="round"/>
        </svg>
    )
  }

  export const RightArrow: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="29" 
            height="50" 
            viewBox="0 0 29 50" 
            fill="none"
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M2 2L26 25L2 48" 
                stroke="#CDCDCD" 
                strokeWidth="3" 
                strokeLinecap="round"/>
        </svg>
    )
  }

  export const ChevronRight: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="24" 
            height="24" 
            viewBox="0 0 24 24" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M10 17L15 12L10 7" 
                stroke="#FBFBFB" 
                strokeWidth="2" 
                strokeLinecap="round"/>
        </svg>
    )
  }

  export const Point: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="9" 
            height="8" 
            viewBox="0 0 9 8" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M4.632 7.728C3.608 7.728 2.73333 7.36533 2.008 6.64C1.28267 5.91467 0.92 5.04 0.92 4.016C0.92 2.97067 1.28267 2.08533 2.008 1.36C2.73333 0.634666 3.608 0.271999 4.632 0.271999C5.67733 0.271999 6.56267 0.634666 7.288 1.36C8.01333 2.08533 8.376 2.97067 8.376 4.016C8.376 5.04 8.01333 5.91467 7.288 6.64C6.56267 7.36533 5.67733 7.728 4.632 7.728Z" 
                fill="#D0D0DB"/>
        </svg>
    )
  }

  export const CircleIn: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="632" 
            height="546"
            viewBox="0 0 632 546" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <circle 
                opacity="0.64" 
                cx="316" 
                cy="230" 
                r="314" 
                stroke="#6952FF" 
                strokeWidth="4"/>
        </svg>
    )
  }

  export const CircleOut: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="768" 
            height="614" 
            viewBox="0 0 768 614" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <circle 
                opacity="0.32" 
                cx="384" 
                cy="230" 
                r="382" stroke="#6952FF" 
                strokeWidth="4"/>
        </svg>
    )
  }

  export const Button: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="44" 
            height="44" 
            viewBox="0 0 44 44" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M20 27L25 22L20 17" 
                stroke="#FBFBFB" 
                strokeWidth="2" 
                strokeLinecap="round"/>
            <rect 
                x="1" 
                y="1" 
                width="42" 
                height="42" 
                rx="9" 
                stroke="#FBFBFB" 
                strokeWidth="2"/>
        </svg>
    )
  }

  export const Symbol: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="116" 
            height="92" 
            viewBox="0 0 116 92" 
            fill="none" 
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M102.332 0.345215C91.9924 14.2931 85.0604 32.9514 80.471 49.0859C73.2676 11.5015 66.4513 2.85415 57.8504 2.85415C49.2496 2.85415 42.3126 11.6205 34.6669 49.9884C30.0824 33.6258 23.1053 14.5509 12.549 0.345215L0.268555 9.20581C19.7976 35.5447 26.0208 79.0593 27.4132 90.9048H43.2878C47.0881 60.7084 53.1102 32.3267 57.8253 21.3637C62.4248 32.3614 68.1604 60.7431 71.654 90.9048H87.4934C88.8657 79.0593 95.0889 35.5397 114.638 9.20581L102.332 0.345215Z" 
                fill="#FBFBFB"/>
        </svg>
    )
  }

  export const TimePoint: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="26" 
            height="26" 
            viewBox="0 0 26 26" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <circle 
                cx="13" 
                cy="13" 
                r="8" 
                fill="#6952FF"/>
            <circle 
                cx="13" 
                cy="13" 
                r="10.5" 
                stroke="#6952FF" 
                strokeOpacity="0.24" 
                strokeWidth="5"/>
            <circle 
                cx="13" 
                cy="13"
                r="3" 
                fill="#FBFBFB"/>
        </svg>
    )
  }

  export const ChevronUp: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="25" 
            height="24" 
            viewBox="0 0 25 24" 
            fill="none"
            className={className} 
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M17.4783 14L12.4783 9L7.47827 14" 
                stroke="#FBFBFB" 
                strokeWidth="2" 
                strokeLinecap="round"/>
        </svg>
    )
  }

  export const ChevronDown: FC<TLogoProps & {selected?: boolean}> = ({className, selected}) => {
    return (
        <svg 
            width="25" 
            height="24" 
            viewBox="0 0 25 24" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <g 
                opacity={selected ? "0.64" : "1"}>
                <path 
                    d="M7.47827 10L12.4783 15L17.4783 10" 
                    stroke="#FBFBFB" 
                    strokeWidth="2" 
                    strokeLinecap="round"/>
            </g>
        </svg>
    )
  }

  export const Burger: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="24" 
            height="24" 
            viewBox="0 0 24 24" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <g 
                opacity="0.9">
            <path 
                d="M2 6C2 5.44772 2.44772 5 3 5H21C21.5523 5 22 5.44772 22 6C22 6.55228 21.5523 7 21 7H3C2.44772 7 2 6.55228 2 6Z" 
                fill="#FBFBFB"/>
            <path 
                d="M2 12C2 11.4477 2.44772 11 3 11H21C21.5523 11 22 11.4477 22 12C22 12.5523 21.5523 13 21 13H3C2.44772 13 2 12.5523 2 12Z" 
                fill="#FBFBFB"/>
            <path 
                d="M3 17C2.44772 17 2 17.4477 2 18C2 18.5523 2.44772 19 3 19H21C21.5523 19 22 18.5523 22 18C22 17.4477 21.5523 17 21 17H3Z" 
                fill="#FBFBFB"/>
            </g>
        </svg>
    )
  }

  export const CloseIcon: FC<TLogoProps> = ({className}) => {
    return (
        <svg 
            width="24" 
            height="24" 
            viewBox="0 0 24 24" 
            fill="none" 
            className={className}
            xmlns="http://www.w3.org/2000/svg">
            <path 
                d="M6.34335 4.92891C5.95283 4.53838 5.31966 4.53838 4.92914 4.92891C4.53862 5.31943 4.53862 5.9526 4.92914 6.34312L10.586 12L4.92912 17.6569C4.5386 18.0474 4.5386 18.6805 4.92912 19.0711C5.31965 19.4616 5.95281 19.4616 6.34334 19.0711L12.0002 13.4142L17.6571 19.071C18.0476 19.4616 18.6808 19.4616 19.0713 19.071C19.4618 18.6805 19.4618 18.0474 19.0713 17.6568L13.4144 12L19.0713 6.34315C19.4618 5.95263 19.4618 5.31946 19.0713 4.92894C18.6807 4.53842 18.0476 4.53842 17.657 4.92894L12.0002 10.5858L6.34335 4.92891Z" 
                fill="#FBFBFB"/>
        </svg>
    )
  }