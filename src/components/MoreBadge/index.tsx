import {ChangeEvent, FC, useState} from "react";
import {
    StyledFillButton,
    StyledImage,
    StyledInfoInnerWrapper,
    StyledInfoWrapper
} from "../../pages/MainPage/styled";
import {Text} from "../Text/Text";
import {Spacer} from "../Spacer/Spacer";
import { BaseField } from "../fields/BaseField/BaseField";
import { PageProps } from "../../data/types";
import { StyledWrapper } from "../FillBadge/styled";
import { useNavigate } from "react-router-dom";
import { ROUTES } from "../../data/constants";


export const MoreBadge: FC<PageProps> = ({isMobile}) => {
    const history = useNavigate()
    const [email, setEmail] = useState('')
    const handleChangeEmail = (e: ChangeEvent<HTMLInputElement>) => {
        setEmail(e.target.value)        
    }
    const handleClearField = () => {
        setEmail('')
    }
    const handleClick = () => {
        setEmail('')        
        history(ROUTES.CONTACT)
    }
    return (
        <>
            {isMobile ? 
            <>
                <StyledWrapper more>
                    <img src="/Default2.svg" alt="def" width={177} height={150}/>
                    <Spacer width='100%' height={24} />
                    <Text size='title24' colorTheme='black100' isBold align='center'>Want to know more about Warp?</Text>
                    <Spacer width='100%' height={8} />
                    <Text size='text16' colorTheme='black100' align='center'>Subscribe and stay up to date with all the news and important information</Text>                    
                    <Spacer width='100%' height={16} />
                    {/* <BaseField
                            onChange={handleChangeEmail}
                            value={email}
                            isMobile
                            name='email'
                            label='Enter your email'
                            onClearField={handleClearField}
                        /> */}
                    <Spacer width='100%' height={16} />
                    <StyledFillButton isMobile onClick={handleClick}>
                        <Text size='text16' colorTheme='black100' align='center'>Subscribe</Text>
                    </StyledFillButton>
                </StyledWrapper> 
            </> :
            <StyledInfoWrapper bc='white300' short>
                <StyledInfoInnerWrapper>
                    <Text size='title40' colorTheme='black100' isBold align='left'>Want to know more about Warp?</Text>
                    <Spacer width='100%' height={12} />  
                    <Text size='text20' colorTheme='black100' align='left'>Subscribe and stay up to date with all the news and important information</Text>
                    <Spacer width='100%' height={36} />                                    
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '12px',
                        width: '100%'          
                    }}>
                        {/* <BaseField
                            onChange={handleChangeEmail}
                            value={email}
                            name='email'
                            label='Enter your email'
                            onClearField={handleClearField}
                        /> */}
                        <StyledFillButton onClick={handleClick}>
                            <Text size='text20' colorTheme='black100' align='center' isBold>Subscribe</Text>
                        </StyledFillButton>
                    </div>
                </StyledInfoInnerWrapper>
                <StyledImage src='/Default2.svg' />
            </StyledInfoWrapper>
            }
        </>
    )
}
