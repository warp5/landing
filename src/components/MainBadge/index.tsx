import {FC, FunctionComponent, useState} from "react";
import {
    StyledClipersInnerWrapper,
    StyledClipersWrapper,
    StyledConnectingButton,
    StyledFillButton,
    StyledImage,
    StyledInfoInnerWrapper,
    StyledInfoWrapper,
    StyledText
} from "../../pages/MainPage/styled";
import {Text} from "../Text/Text";
import {Spacer} from "../Spacer/Spacer";
import {theme} from "../../data/colors";
import { ROUTES, TProduct } from "../../data/constants";
import { Products } from "../Products";
import { useNavigate } from "react-router-dom";
import Pdf from "../../filles/WarpWDAsking.pdf"
import styled from "@emotion/styled";
import { SwipeableDrawer, SwipeableDrawerProps } from "@mui/material";
import { Symbol } from "../Icons";

type TMainBadgeProps = {
    title?: string,
    text?: string,
    img?: string,
    color: keyof typeof theme["colors"],
    button?: string,
    filled?: boolean,
    description?: string, 
    short?: boolean,
    textColor?: keyof typeof theme["colors"],
    labels?: TProduct[],
    withContact?: string,
    isMobile?: boolean,
    main?: boolean
}

export const MainBadge: FC<TMainBadgeProps> = ({
    title, 
    text, 
    img, 
    color, 
    button, 
    filled = true, 
    description, 
    short = false, 
    textColor, 
    labels, 
    withContact,
    isMobile = false,
    main = false
}) => {
    const history = useNavigate()
    const handleClick = () => {
        if (button?.toLowerCase().includes('about')) history(ROUTES.ABOUT)
        if (button?.toLowerCase().includes('fill') || button?.toLowerCase().includes('contact')) history(ROUTES.CONTACT)
        if (button?.toLowerCase().includes('connect')) {
            if (title?.toLowerCase().includes('clipers'))
                window.open('https://clipers.ru')
            else
                scrollToBottom()
        }
        if (button?.toLowerCase().includes("pitch")) window.open(Pdf)        
    }

    const scrollToBottom = () =>{
        window.scrollTo({
          top: 3400, 
          behavior: 'smooth'
        })
    }

    const [modal, setModal] = useState(false)
    const handleClickContact = () => {
        if (withContact?.includes('Demo'))
            window.open('https://youtube.com/shorts/3P0-gfrLpNc?feature=share')
        else if (withContact?.includes('Memo'))
            handleOpenModal()
        else
            history(ROUTES.CONTACT)
    }
    const handleOpenModal = () => {
        setModal(true)
    }
    const handleCloseModal = () => {
        setModal(false)
    }

    return (
        <>
            <StyledInfoWrapper bc={color} short={short}>
                <StyledInfoInnerWrapper isMobile={isMobile}>
                    {isMobile && main ? 
                    <>
                        <img 
                            src="/products.svg"
                            alt="products"
                            /> 
                        <Spacer width='100%' height={16} />    
                    </>: 
                    <></>}
                    <Text size={isMobile ? 'title40' : 'title64'} colorTheme={textColor ? textColor : 'black'} isBold align='center'>{title}</Text>
                    <Spacer width='100%' height={16} />
                    {text ? <StyledText size={isMobile ? 'text16' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='center'>{text}</StyledText> : <></>}
                    <Spacer width='100%' height={8} />
                    {description ? <Text size={isMobile ? 'title24' : 'title32'} colorTheme={textColor ? textColor : 'black'} align='center'>{description}</Text> : <></>}
                    <Spacer width='100%' height={isMobile ? 24 : 36} />
                    {(button && !withContact) ? filled ?
                        <StyledConnectingButton onClick={handleClick} isMobile={isMobile}>
                            <Text size={isMobile ? 'text16' : 'text20'} colorTheme='white100' align='center' isBold>{button}</Text>
                        </StyledConnectingButton> :
                        <StyledFillButton onClick={handleClick} isMobile={isMobile}>
                            <Text size={isMobile ? 'text16' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='center' isBold>{button}</Text>
                        </StyledFillButton> : <></>
                    }
                    {withContact ? 
                    <div style={{
                        display: 'flex',
                        width: isMobile ? '100%' : '50%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        gap: '16px',
                        zIndex: '20',
                        flexDirection: isMobile ? 'column' : 'row'
                    }}>
                        <StyledConnectingButton onClick={handleClick} isMobile={isMobile}>
                            <Text size={isMobile ? 'text16' : 'text20'} colorTheme='white100' align='center' isBold>{button}</Text>
                        </StyledConnectingButton>
                        <StyledFillButton onClick={handleClickContact} isMobile={isMobile}>
                            <Text size={isMobile ? 'text16' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='center' isBold>{withContact}</Text>
                        </StyledFillButton>
                    </div> :
                    <></>}
                </StyledInfoInnerWrapper>
                {img ? <StyledImage src={img} /> : <></>}
                {labels && labels.length ? 
                <Products list={labels} noFont/> :
                <></>}
            </StyledInfoWrapper> 
            {title?.toLowerCase().includes('clipers') ? 
             <StyledClipersWrapper>
                <StyledClipersInnerWrapper isMobile={isMobile}>
                    <div>
                        <Text size={isMobile ? 'title24' : 'title40'} colorTheme={textColor ? textColor : 'black'} align='left' isBold>What we offer to a <Text size={isMobile ? 'title24' : 'title40'} colorTheme='brand100' as='span' isBold>client</Text></Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}convenient short videos format for presenting information</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}AI-based relevant offer feed</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}before order, evaluate best service, see the real reviews of users by videos</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}make any order of the service in a few clicks</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}smart reminders (it's time to:order food, book a barber, etc.)</Text>
                    </div>
                    <img src="/ClipForClient.png" alt="im" width={isMobile ? 150 : 180} height={isMobile ? 300 : 370}/>
                </StyledClipersInnerWrapper>
                <Spacer width='100%' height={40} />
                <StyledClipersInnerWrapper>
                    <img src="/ClipForBus.png" alt="im" width={isMobile ? 150 : 180} height={isMobile ? 300 : 370}/>
                    <div>
                        <Text size={isMobile ? 'title24' : 'title40'} colorTheme={textColor ? textColor : 'black'} align='left' isBold>What we offer to a <Text size={isMobile ? 'title24' : 'title40'} colorTheme='brand100' as='span' isBold>business</Text></Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}using the functionality of CRM, register and fully run a business</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}build dashboards and analyze, generate reporting documents</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}flexibly promote your services/products through video</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}simplified integration for ready-to-go business</Text>
                        <Spacer width='100%' height={8} />
                        <Text size={isMobile ? 'text12' : 'text20'} colorTheme={textColor ? textColor : 'black'} align='left' isBold><StyledBullet />{' '}send notifications and personal offers in video format</Text>                                               
                    </div>                    
                </StyledClipersInnerWrapper>                
             </StyledClipersWrapper> :
            <></>}
            <MemoModal 
                open={modal}
                onOpen={handleOpenModal}
                onClose={handleCloseModal}
                isMobile={isMobile}
            />
        </>       
    )
}


const Bullet: FunctionComponent<{className?: string}> = ({className}) => {
    return (
        <svg className={className} width="6" height="28" viewBox="0 0 6 28" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="3" cy="14" r="3" fill="#FF1555"/>
        </svg>
    )
}

const StyledBullet = styled(Bullet)`
    margin-bottom: -10px;
`

type TModalProps = {
    open: SwipeableDrawerProps['open']
    onClose: SwipeableDrawerProps['onClose']
    onOpen: SwipeableDrawerProps['onOpen']
    isMobile: boolean
}  

export const StyledSwipeableDrawer = styled(
    SwipeableDrawer
  )<SwipeableDrawerProps & {isMobile?: boolean}>`
    & .MuiDrawer-paper {
        border-radius: 15px;   
        padding: 14px 40px 14px;
        justify-content: flex-start;
        align-items: center;
        overflow: visible;
        margin-bottom: 35vh;
        margin-left: ${({isMobile}) => isMobile ? '2.5vw' : '27vw'};
        width: ${({isMobile}) => isMobile ? '95' : '45'}%; 
    }
  `

const MemoModal: FC<TModalProps> = (props) => {
    return (
        <StyledSwipeableDrawer
            onOpen={props.onOpen}
            open={props.open}
            onClose={props.onClose}
            anchor='bottom'
            disableSwipeToOpen
            isMobile={props.isMobile}
        >
            <div style={{
                display: 'flex',
                padding: '40px',
                backgroundColor: theme.colors.darkBlue,
                borderRadius: '12px'
            }}>
            <Symbol />
            </div>
            <Spacer width='100%' height={12} />
            <Text size={props.isMobile ? 'title24' : 'title64'} colorTheme='black' align='center' isBold>Warp Universe OÜ</Text>
            <Spacer width='100%' height={12} />
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                gap: '12px',
                justifyContent: 'space-between',
                width: '100%'
            }}>
                <Text size={props.isMobile ? 'text16' : 'text20'} colorTheme='black' align='left' isBold>Address:</Text>            
                <Text size={props.isMobile ? 'text16' : 'text20'} colorTheme='black' align='right' isBold>Narva mnt 5-1, 10117, Tallinn, Estonia</Text>
            </div>
            <Spacer width='100%' height={12} />
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                gap: '14px',
                justifyContent: 'space-between',
                width: '100%'
            }}>
                <Text size={props.isMobile ? 'text16' : 'text20'} colorTheme='black' align='left' isBold>Registration number:</Text>            
                <Text size={props.isMobile ? 'text16' : 'text20'} colorTheme='black' align='right' isBold>16453494</Text>
            </div>
            <Spacer width='100%' height={12} />
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                gap: '14px',
                justifyContent: 'space-between',
                width: '100%'
            }}>
                <Text size={props.isMobile ? 'text16' : 'text20'} colorTheme='black' align='left' isBold>Managing Director:</Text>            
                <Text size={props.isMobile ? 'text16' : 'text20'} colorTheme='black' align='right' isBold>Oksana Golovina</Text>
            </div>
            <Spacer width='100%' height={12} />
        </StyledSwipeableDrawer>
    )
}