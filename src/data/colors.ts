const COLORS = {
    white300: '#ECECEC',
    white600: '#F9F9F9',
    brand100: '#FF1555',
    brand200: '#E51F54',
    link: '#0B63EE',
    error: '#E43802',
    help: '#CB1B10',
    success: '#2CA940',
    tag_dark: '#182C60',
    ////warp colors
    primary100: '#6952FF',
    lightPallet: '#FFB6E6',
    white200: '#F8F8F9',
    white500: '#D0D0DB',
    white100: '#FBFBFB',
    white400: '#E6E8EB',
    black300: '#242427',
    black: '#000000',
    black200: '#18181A',
    black500: '#3C3B42',
    black100: '#121213',
    deepOrange: '#FF4125',
    lightOrange: '#FF761B',
    darkGreen: '#334D01',
    darkBlue: '#003673',
    pink: '#FF3880',
    darkAqua: '#003636',
    lightPink: '#FFB6E6',
    green: '#3DE85C',
    yellow: '#FFBF34',
    black400: '#2A2A2E'
}

export const theme = {
    colors: COLORS
}
