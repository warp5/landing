import { FunctionComponent } from "react"
import { DefaultLogo } from "../components/Icons"
import { theme } from "./colors"

export const ROUTES = {
    MAIN: '/',
    ABOUT: '/about',
    CONTACT: '/contact',
    INVESTORS: '/investors',
    PARTNERS: '/partners',
    PRODUCTS: '/products/:type',
    POLICY: '/documents/policy',
    TERMS: '/documents/terms'
}

export type TMenu = {
    title: string,
    route: string,
    type?: number,
    dropDown?: TMenu[]
}
export const MENU:TMenu[] = [
    {
        title: 'About',
        route: ROUTES.ABOUT
    },
    {
        title: 'Products',
        route: ROUTES.PRODUCTS,
        dropDown: [
            {
                title: 'Platform',
                route: ROUTES.PRODUCTS,
                type: 0   
            },
            {
                title: 'Channels',
                route: ROUTES.PRODUCTS,
                type: 1   
            },
            {
                title: 'Content',
                route: ROUTES.PRODUCTS,
                type: 2     
            },
            {
                title: 'Twin',
                route: ROUTES.PRODUCTS,
                type: 3        
            },
            {
                title: 'Coin',
                route: ROUTES.PRODUCTS,
                type: 4       
            },
            {
                title: 'Meta',
                route: ROUTES.PRODUCTS,
                type: 5       
            },
            {
                title: 'Avatar',
                route: ROUTES.PRODUCTS,
                type: 6        
            },
            {
                title: 'Social',
                route: ROUTES.PRODUCTS,
                type: 7
            }
        ]
    },
    {
        title: 'Partners and Suppliers',
        route: ROUTES.PARTNERS
    },
    {
        title: 'Investors',
        route: ROUTES.INVESTORS
    }
]

export type TGrid = {
    title: string,
    text?: string,
    point?: string,
    link1?: string,
    link2?: string,
    Icon?: FunctionComponent
}

type TGood = {
    title: string,
    text: string
}

export type TProduct = {
    title?: string,
    text?: string,
    img?: string,
    point: string,
    absolute: boolean,
    color: keyof typeof theme['colors'],
    marginRight?: string,
    marginLeft?: string,
    marginTop?: string,
    marginBottom?: string,
    goods?: TGood[],
    type?: number
}

export const LABELS:TProduct[] = [
    {
       color: 'lightOrange',
       absolute: true,
       point: 'Tickets', 
       marginLeft: '-1100px',
       marginBottom: '200px'
    },
    {
        color: 'lightPink',
        absolute: true,
        point: 'Transfer', 
        marginRight: '800px',
        marginTop: '250px'        
     },
     {
        color: 'green',
        absolute: true,
        marginLeft: '1100px',
        point: 'Maps' 
     },
     {
        color: 'yellow',
        absolute: true,
        point: 'Flat',
        marginLeft: '800px',
        marginTop: '300px' 
     }
]

export const PRODUCTS:TProduct[] = [
    {
        title: 'Warp Platform',
        text: 'A marketplace where users who want to plan and manage all travel needs in one place meet travel service providers who wants to offer variations of opportunities and products for millions of potential clients',
        type: 0,
        img: '/Default2.svg',
        point: 'Warp Platform',
        absolute: false,
        color: 'primary100',
        goods: [
            {
                title: 'Core Travel Technologie',
                text: 'Works with all classic leisure and business travel players - airlines, accommodation, metasearch and wraps all offers in one ready solution which fits best consumer needs'
            },
            {
                title: '85 modules',
                text: '“Plug-and-play” solution, ready to connect to different travel service providers around the globe in all areas'
            },
            {
                title: 'Provides full cycle product management',
                text: 'One contact point for user and customers in terms of payments, dynamic pricing, problem solving, etc.'
            },
            {
                title: 'Extremely fast',
                text: 'Platform is ready to process more than 20000 requests, 200 bookings and more than 51 billion pricing calculations per second without critical load'
            },
            {
                title: 'AI driven algorithms',
                text: 'AI is used for non-stop market analysis, pricing forecasts and dynamic packaging of millions of offers'
            },
            {
                title: 'Infinite scalability',
                text: 'Ready to work with endless number users and partners as well as types of travel services '
            }
        ]
    },
    {
        title: 'Warp Channels',
        text: 'Pellentesque mattis dictum. Venenatis molestie nunc augue eleifend leo, sit amet, dictumst. Ultricies. Pulvinar interdum molestie non ultricies. Et interdum efficitur tortor, urna sit non sapien et sodales arcu leo',
        type: 1,
        img: '/Default.svg',
        point: 'Channels',
        absolute: true,
        color: 'darkAqua',
        marginLeft: '692px',
        marginBottom: '300px',
        goods: [
            {
                title: 'Mobile all-in-one SuperApp',
                text: 'Main channel of interaction with customers, light and fast app which adjust its interface based on needs and location'
            },
            {
                title: 'Web Marketplace',
                text: 'Main connection point between platform and partners where they can offer their travel-related services to be wrapped into packages and offered to users'
            },
            {
                title: 'Open API',
                text: 'Easy integration into any proprietary platform as well as all major market players'
            },
            {
                title: 'Business travel solution',
                text: 'Foe Business Travel Agencies and Corporate Clients (incl. governmental) with efficient pricing, revenue and pax management and distribution tool for the travel suppliers.'
            },
            {
                title: 'Extranet',
                text: 'Access to millions of guests every day, leisure or business travelers, easy adjustment of any offer criteria from prices and tariffs to special offers. we treat all scales of businesses equally - from private apartments to big hotel chains'
            },
            {
                title: 'B2B workspace',
                text: 'All types of travel content available in one window, seamlessly bookable within one transaction at best prices and highest earnings on sales'
            },
            {
                title: 'Metaverses',
                text: 'New touchpoint where partners can promote their services and offers to increase awareness and sales using different kinds of entertaining mechanics'
            }
        ]
    },
    {
        title: 'Warp Content',
        text: 'Our Platform includes variety of content and solutions both for making its functioning fast, responsive and stable as well as for accurate gathering, packaging and promoting of all kinds of services and products on B2C, B2B & B2G segements',
        type: 2,
        img: '/Default.svg',
        point: 'Content',
        absolute: true,
        color: 'darkGreen',
        marginLeft: '50px',
        marginTop: '560px',
        goods: [
            {
                title: 'CMS – Content management system',
                text: 'Automated aggregation of information about tourist products, services and objects with full descriptions, photos, videos, real-time prices, restrictions and attributes at any moment in time'
            },
            {
                title: 'CDN – Content distribution network',
                text: 'All data is distributed across data centers around the globe so that it could accessible instantly from any point. Data backup ensures no losses of information'
            },
            {
                title: 'Routes Open platform',
                text: 'Combination of any route directions in real time provided with recommendations «where to go» and «the fastest way to get» based on the current client’s location. Routes can be created by any of the Warp’s authorized members'
            },
            {
                title: 'AR/VR studio',
                text: 'Dedicated instruments for AR/VR content construction where service providers can mark their placed and make them visible for tourists '
            },
            {
                title: 'Marketing and ads platform',
                text: 'Boundless possibilities to advertise and promote places, products and services among millions of tourusts around the globe'
            }
        ]
    },
    {
        title: 'Warp Twin',
        text: 'The platform is designed to store digital copy of objects (artificial and natural), buildings, streets and other infrastructure up to continent scale. Each object is stored in the cloud database and has unlimited number of attributes that describe its properties and parameters',
        type: 3,
        img: '/Default.svg',
        point: 'Twin',
        absolute: true,
        color: 'pink',
        marginLeft: '750px',
        goods: [
            {
                title: 'User-Urban Interface protocol',
                text: 'Web interfaces and mobile application which allows users to enrich information about specific objects, connect objects into routes, add descriptions to waypoints in various ways: textual information, images, video, expanded reality (XR).'
            },
            {
                title: 'Devices laboratory',
                text: 'Where we create and test various devices, controllers and aps for interaction with Users and Partners e.g. AR/VR/XR content, uploading of offers, programming of Avatars and etc.'
            },
            {
                title: '3D-copy of environment',
                text: 'Special tools for easy creation of detailed 3D copies of objects of different scales for AR/VR which involves direct user interaction with urban infrastructure facilities'
            },
            {
                title: 'IoT platform',
                text: 'This platform serves for the interaction of objects equipped with various controllers and Warp devices with each other and with user devices that support User-Urban Interface Protocol'
            }
        ]
    },
    {
        title: 'Warp Coin',
        text: 'New era in in-app purchases. Truly global currency, exchangable and convertable with no limitations in terms of spending for all kinds of touristic servives in contarst with hundreds of different “miles” and “points”.',
        type: 4,
        img: '/Default.svg',
        point: 'Coin',
        absolute: true,
        color: 'lightOrange',
        marginRight: '550px',
        marginTop: '350px',
        goods: [
            {
                title: 'New Economy',
                text: 'In-app currency which will dramatically change touristic industry landscape as it wll be used across different services from airlines and hotels to revealing of secret palces and hidden gems.'
            },
            {
                title: 'Base for loyalty program',
                text: 'Clients will collect Warp Coins for different activities like posting reviews, providing recomndations and adding of any kinds of USG, as well as for sussessful accommlishment of in-app games and quests.'
            },
            {
                title: 'Freely convertible currency',
                text: 'One currency which allows to to pay for for services around the globe as well as be converted into real currencies and vice versa.'
            },
            {
                title: 'Pay with Warp.Coin for everything',
                text: 'Clients will be able to spend Warp Coins for in-app purchases including payments for travel and accomodation as well as for opening special “secret” content'
            }
        ]
    },
    {
        title: 'Warp Meta',
        text: 'Step by step Warp will create Metaverse of real objects, starting from hotels and resorts and up to streets, cities, countries and continents. These will be real copies of existing objects where partners and service providers will be able to own copies of their real properties and customers will benefit from advance planning of their trips and sightseeing',
        type: 5,
        img: '/Default.svg',
        point: 'Metaverse',
        absolute: true,
        color: 'deepOrange',
        marginRight: '750px',
        goods: [
            {
                title: 'Copy of the real world',
                text: 'Rear Meta universe with 3D-copies of real existing objects with high detalization'
            },
            {
                title: 'AAA+ game',
                text: 'Real quests in open-world locations with outstanding graphics on the palm of your hand'
            },
            {
                title: 'Scenario of the Game',
                text: 'Possibility to generate different game scenarios based on request of owners of objects in Metaverse'
            },
            {
                title: 'Worldwide quest',
                text: 'Open-world, no limits exploring different towns, countries or even continents'
            },
            {
                title: 'NFT and Crypto exchange',
                text: 'Real in-game currency which can be earned bu passing various tasks and exchanged for in-app or real world benefits'
            },
            {
                title: 'Any entertainment mechanics',
                text: 'No limits in game mechanics - pass special tasks, find hidden gems, explore and share information with friend and earn Warp Coins'
            },
            {
                title: 'Different levels of investments',
                text: 'Partners and service providers can buy and own objects in Warp Metaverse staring from the small shop or apartment and up to buildings and stereets or airports'
            },
            {
                title: 'Decentralized world',
                text: 'Customers will be able to visit thousand of locations in Warp Metaverse and find the most interesting places for them to be visited in reality. Just guess which locations they will visit first if they have seen them before in Meta!'
            }
        ]
    },
    {
        title: 'Warp Avatars',
        text: 'New era of digital concierges - from voice assistance to real persons who can present your business from small souvenir shop to big resort. Guided tours, digital concierge - it’s limited only bu your imagination.',
        type: 6,
        img: '/Default.svg',
        point: 'Avatars',
        absolute: true,
        color: 'darkBlue',
        marginLeft: '620px',
        marginTop: '300px',
        goods: [
            {
                title: 'Digital concirege',
                text: 'Technology that allows business to create its own memorable brand representation, increases guests\' engagement, reduces the workload on staff'
            },
            {
                title: 'Official digital representation',
                text: 'Not only business, but even  on a country or governmental level you can create your digital representation in Warp Metaverse'
            },
            {
                title: 'Religious and cultural tourism in Metaverse',
                text: 'Excellent tool in combination with Warp Metaverse which allow tourists to visit iconic places and listen to digital guides. Those digital visits will create strong intention to visit these places in real world'
            },
            {
                title: 'Augmented reality as well as usual app',
                text: 'Not only Metaverse but also in normal 2D partners and service providers will be able to incorporate Avatar technology to represent any kind of of business the way you wish'
            }
        ]
    },
    {
        title: 'Warp Clipers',
        text: 'Lifestyle services social platform which has already registered more than 1,000 users, including opinion leaders with a total audience in other social networks of more than 500,000 people',
        type: 7,
        img: '/Default.svg',
        point: 'Social',
        absolute: true,
        color: 'brand100',
        marginRight: '750px',
        marginBottom: '300px'        
    }
]

export type TFAQ = {
    index: number,
    title: string,
    answers?: TFAQ[]
}

export const FAQ:TFAQ[] = [
    {
        title: "Is Warp just another aggregation tool and marketplace in travel business?",
        index: 0,
        answers: [
            {
                title: "No, the whole concept of Warp is completely different from existing travel aggregators. Warp provides to travelers variations of ready packages based on their needs at the best possible price. It’s not only flights and accommodation but many other services which will bring travel experience to the new level: detailed hotel information, information on leisure activities at the destination place, possibility to see place of travel in advance using AR/VR and many more.",
                index: 0
            }
        ]
    },
    {
        title: "What is unique about Warp?",
        index: 1,
        answers: [
            {
                title: "Warp was created by the team of professionals in touristic and travel business. We know all smallest details about travel industry and that is why we’ve created platform which combines much more that flights and accommodation. But even these basic things we brought to the new level in terms of variations, speed of processing, dynamic pricing and user experience both for Clients and Service Providers.",
                index: 0
            }
        ]
    },
    {
        title: "When Warp will be launched and where?",
        index: 2,
        answers: [
            {
                title: "Warp will be launched in summer 2023 with different services in different countries and will become global platform in 2024. First launch markets will be Russian Federations (isolated), North America, Baltic Countries, Scandinavia, Georgia, UAE and Saudi Arabia",
                index: 0
            }
        ]
    },
    {
        title: "What I as a travel Partner/Supplier will get from cooperation with Warp?",
        index: 3,
        answers: [
            {
                title: "You will be able to present your business to millions of customers all over the world. Warp will do a lot of automation for your business in terms of packaging and price suggestions. In addition, you will be able to use Warp’s recourses to digitalize your business in areas which you’ve probably never thought about before like Metaverse, Twin, Avatars and AR/VR",
                index: 0
            }
        ]
    },
    {
        title: "What business can be connected to Warp Platform?",
        index: 4,
        answers: [
            {
                title: "All types of travel/touristic businesses of any scale. You will be able to use either our APIs or web interface or App to connect depending on your needs and possibilities"                ,
                index: 0
            }
        ]
    }
]

export const FAQ_INV:TFAQ[] = [
    {
        title: "Is Warp on a “PDF startup” stage?",
        index: 0,
        answers: [
            {
                title: "No, Warp was founded several years ago on has well-developed proprietary IT system which has proven its efficiency in real life conditions. Warp has received funding and now is on the pre-launch phase. Current value of IT assets is close to USD 1 bln.",
                index: 0
            }
        ]
    },
    {
        title: "What is Warp?",
        index: 1,
        answers: [
            {
                title: "Warp is universal solution in innovative travel-tech and digital/smart city spheres ready for B2C, B2B and B2G segments. Warp product line-up consists of travel marketplace, solution for creating GIS on governmental level, digital concierge services, content platform with AR/VR, digital twins of place/city/region/country, game mechanics, internal currency (Warp Coin) and Metaverse (copy of the real world)",
                index: 0
            }
        ]
    },
    {
        title: "When Warp will be launched and where?",
        index: 2,
        answers: [
            {
                title: "Warp will be launched in summer 2023 with different services in different countries and will become global platform in 2024. First launch markets will be Russian Federations (isolated), North America, Baltic Countries, Scandinavia, Georgia, UAE and Saudi Arabia",
                index: 0
            }
        ]
    },
    {
        title: "How can I invest in Warp?",
        index: 3,
        answers: [
            {
                title: "Currently it is possible to invest into platform and instruments development. Investment is possible in Warp Holding which is based in the US. Minimum level of investment starts from USD 15 mln. Investment return is expected in 1,5 – 3 years timeframe with up to 5 times growth. Possible profit is expected to be received from shares sales, dividends or IPO.",
                index: 0
            }
        ]
    }
]


export type TInfo = {
    title: string,
    text: string,
    position: 'left' | 'right'
}

export const INFO: TInfo[] = [
    {
        title: '1',
        text: 'Warp Platform is a fundamental instrument for GIS in travel and service, allowing to digitize and manage whole industry',
        position: 'left'
    },
    {
        title: '2',
        text: 'Warp Marketplace which is international omni-channel high-tech marketplace of an endless number of local travel & service products, providing exceptional customer experience ever',
        position: 'right'
    },
    {
        title: '3',
        text: '6 core products built around Warp Platform which expand travel industry to a completely new level of accessibility',
        position: 'left'
    },
    {
        title: '4',
        text: 'SuperApp which provides to Users and Partners easy access to Warp platform and Products at their fingertips',
        position: 'right'
    },
    {
        title: '5',
        text: 'Proprietary technology under the hood with extremely rapid price combinations recalculation, ready for high loads, endlessly scalable and cross-platform ready',
        position: 'left'
    }
]

export const ABOUT_INFO: TInfo[] = [
    {
        title: '1',
        text: 'Digital platform  for all  types of Business Travel Agencies and Corporate Clients',
        position: 'left'
    },
    {
        title: '2',
        text: 'Supplying best-priced multivertical travel content with the seamless booking engine for the OTAs and other digital travel players',
        position: 'right'
    },
    {
        title: '3',
        text: 'Online travel product and seamless booking engine for offline travel retailers',
        position: 'left'
    },
    {
        title: '4',
        text: 'Efficient pricing, revenue and pax management and distribution tool for the travel suppliers',
        position: 'right'
    },
    {
        title: '5',
        text: 'Upsales of the complimentary products and services',
        position: 'left'
    }
]


export const PARTNERS_INFO: TInfo[] = [
    {
        title: '1.',
        text: 'API integration with existing and well-established travel solutions',
        position: 'left'
    },
    {
        title: '2.',
        text: 'Web interface for manual or automatic offers upload and download',
        position: 'right'
    },
    {
        title: '3.',
        text: 'Mobile app for smaller businesses and service providers',
        position: 'left'
    }
]

export const CONTACTS: TGrid[] = [
    {
        Icon: DefaultLogo,
        title: '10117, Estonia, Tallinn, Narva mnt. 5'
    },
    {
        Icon: DefaultLogo,
        title: '+371 283 54789'
    },
    {
        Icon: DefaultLogo,
        title: 'info@warp.com'
    },
    {
        Icon: DefaultLogo,
        title: 'LinkedIn',
        link1: 'https://www.linkedin.com/company/warp-universe/'
    },
    {
        Icon: DefaultLogo,
        title: 'Telegram',
        link1: "https://t.me/Warp_Universe"

    },
    {
        Icon: DefaultLogo,
        title: 'WhatsUp',        
        link1: "https://wa.me/message/I2LMELP4KYUDH1"
    },
    {
        Icon: DefaultLogo,
        title: 'Twitter',
        link1: 'https://twitter.com/Warp_Universe'
    }
]


export const TECHOLOGIES = [
    {
        title: 'Extremally rapid price combinations recalculation',
        text: '> 51 billions combo per second'
    },
    {
        title: 'Endlessly scalable'
    },
    {
        title: 'Cross-platform solution'
    },
    {
        title: 'Extremally fast price combinations calculation',
        text: '> 51 billions combo per second'
    }
]

export const PARTNERS:TGrid[] = [
    {
        title: 'Suppliers',
        text: '“New black” salesforce via worldwide B2C / B2B / B2G channels'
    },
    {
        title: 'Distributors',
        text: 'Multivertical product and revenue management tool in one solution'
    },
    {
        title: 'Touroperators & Wholesalers',
        text: 'The best prices and all travel products in one place'
    },
    {
        title: 'Online & offline travel agencies',
        text: '“New black” salesforce via worldwide B2C / B2B / B2G channels'
    },
    {
        title: 'Others allied businesses',
        text: 'Your new markets -  salesforce, clients, and partners'
    }
]

export type TSwipGrid = {
    text: string,
    author: string,
    position: number
}

export const SUCCESS_STORY:TSwipGrid[] = [
    {
        text: "Urna ipsum morbi cursus consectetur ultricies. Dolor malesuada nec urna quis, cras malesuada orci, vestibulum urna pulvinar dui accumsan justo venenatis dictumst. Nisi ex. Urna et nulla aenean dolor.",
        author: "Andy Thompson",
        position: 0
    },
    {
        text: "Urna ipsum morbi cursus consectetur ultricies. Dolor malesuada nec urna quis, cras malesuada orci, vestibulum urna pulvinar dui accumsan justo venenatis dictumst. Nisi ex. Urna et nulla aenean dolor.",
        author: "Andy Thompson",
        position: 1
    },
    {
        text: "Urna ipsum morbi cursus consectetur ultricies. Dolor malesuada nec urna quis, cras malesuada orci, vestibulum urna pulvinar dui accumsan justo venenatis dictumst. Nisi ex. Urna et nulla aenean dolor.",
        author: "Andy Thompson",
        position: 2
    }
]
