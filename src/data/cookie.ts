export const setCookie = (
    name: string,
    value: string,
    options: { expires?: Date | string; path?: string; 'max-age'?: number } = {}
  ) => {
    options = {
      path: '/',      
      ...options
    }
  
    if (options.expires instanceof Date) {
      options.expires = options.expires.toUTCString()
    }
  
    let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value)
  
    for (let optionKey in options) {
      updatedCookie += '; ' + optionKey
      // @ts-ignore
      let optionValue = options[optionKey]
      if (optionValue !== true) {
        updatedCookie += '=' + optionValue
      }
    }
    document.cookie = updatedCookie
    if (value && value !== '' && name !== 'disablePreview')
      localStorage.setItem(name, value)
  }